﻿using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;

using System.Linq;
using System.Threading;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class ExchangeTest : TransactionRepositoryTest
    {
        private string fiatAssetId = "EUR";

        public ExchangeTest()
        {
            //AccountContext.IsTestEnvironment = true;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }

        private void MakeCryptoDeposit(TransactionRepository transactionRepository, string accountId, decimal amount)
        {
            Transaction depositTransaction = transactionRepository.CreateDepositCryptoTransaction(accountId, amount, 10000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
        }

        [Fact]
        public void FixCoinBuyTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1,
                    Price = 10500,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10500, transactionDetail.FiatAmountTotal);
            Assert.Equal(10500, transactionDetail.FiatAmountSubtotal);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(-10500, fiatEntry.Amount);
            Assert.Equal('S', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(1, cryptoEntry.Amount);
            Assert.Equal('B', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.003m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1,
                    Price = 10500,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.FixCoin
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(31.88m, transactionDetail.FiatFee, 2);
            Assert.Equal(10657.50m, transactionDetail.FiatAmountTotal.Value, 2);
            Assert.Equal(10625.62m, transactionDetail.FiatAmountSubtotal.Value, 2);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(157.50m, feeTransactionDetail1.FiatFee, 2);
            Assert.Equal(10657.50m, feeTransactionDetail1.FiatAmountTotal.Value, 2);
            Assert.Equal(10500.00m, feeTransactionDetail1.FiatAmountSubtotal.Value, 2);
            Assert.Equal(157.50m, feeTransactionDetail1.TransactionEntry.First().Amount, 2);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }

        [Fact]
        public void FixCoinSellTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            MakeCryptoDeposit(transactionRepository, TestAccounts.Account3BTC, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1,
                    Price = 10000,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    TradeType = TradeType.FixCoin
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(10000, fiatEntry.Amount);
            Assert.Equal('B', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(-1, cryptoEntry.Amount);
            Assert.Equal('S', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.003m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1,
                    Price = 10000,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.FixCoin
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(29.64m, transactionDetail.FiatFee, 2);
            Assert.Equal(9850, transactionDetail.FiatAmountTotal.Value, 2);
            Assert.Equal(9879.64m, transactionDetail.FiatAmountSubtotal.Value, 2);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(150m, feeTransactionDetail1.FiatFee, 2);
            Assert.Equal(9850m, feeTransactionDetail1.FiatAmountTotal.Value, 2);
            Assert.Equal(10000.00m, feeTransactionDetail1.FiatAmountSubtotal.Value, 2);
            Assert.Equal(150m, feeTransactionDetail1.TransactionEntry.First().Amount, 2);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }

        [Fact]
        public void FixFiatBuyTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 0.985m,
                    Price = 10152.28m,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    TradeType = TradeType.FixFiat
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountTotal.Value, 2);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal.Value, 2);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(-10000, fiatEntry.Amount, 2);
            Assert.Equal('S', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(0.985m, cryptoEntry.Amount, 8);
            Assert.Equal('B', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.003m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 0.985m,
                    Price = 10000,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.FixFiat
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(30m, transactionDetail.FiatFee);
            Assert.Equal(10030, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000m, transactionDetail.FiatAmountSubtotal);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(150m, feeTransactionDetail1.FiatFee);
            Assert.Equal(10150m, feeTransactionDetail1.FiatAmountTotal);
            Assert.Equal(10000.00m, feeTransactionDetail1.FiatAmountSubtotal);
            Assert.Equal(150m, feeTransactionDetail1.TransactionEntry.First().Amount);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }

        [Fact]
        public void FixFiatSellTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            MakeCryptoDeposit(transactionRepository, TestAccounts.Account3BTC, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1.015m,
                    Price = 9852.22m,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    TradeType = TradeType.FixFiat
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountTotal.Value, 2);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal.Value, 2);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(10000, fiatEntry.Amount, 2);
            Assert.Equal('B', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(-1.015m, cryptoEntry.Amount, 8);
            Assert.Equal('S', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.003m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1.015m,
                    Price = 10000,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.FixFiat
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(30m, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal);
            Assert.Equal(9970, transactionDetail.FiatAmountTotal);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(150m, feeTransactionDetail1.FiatFee);
            Assert.Equal(9850, feeTransactionDetail1.FiatAmountTotal);
            Assert.Equal(10000.00m, feeTransactionDetail1.FiatAmountSubtotal);
            Assert.Equal(150m, feeTransactionDetail1.TransactionEntry.First().Amount);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }

        [Fact]
        public void LimitBuyTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1m,
                    Price = 10000m,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    TradeType = TradeType.Limit
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(-10000, fiatEntry.Amount);
            Assert.Equal('S', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(1m, cryptoEntry.Amount);
            Assert.Equal('B', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1m,
                    Price = 10000,
                    IsBuy = true,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.Limit
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(150m, transactionDetail.FiatFee);
            Assert.Equal(10150, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000m, transactionDetail.FiatAmountSubtotal);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(150m, feeTransactionDetail1.FiatFee);
            Assert.Equal(10150m, feeTransactionDetail1.FiatAmountTotal);
            Assert.Equal(10000.00m, feeTransactionDetail1.FiatAmountSubtotal);
            Assert.Equal(150m, feeTransactionDetail1.TransactionEntry.First().Amount);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }

        [Fact]
        public void LimitSellTest()
        {
            TransactionRepository transactionRepository = GetTransactionRepository();

            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 100000);
            var executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1m,
                    Price = 10000m,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    TradeType = TradeType.Limit
                }
            };

            Transaction exchangeTransaction = transactionRepository.CreateTradeTransaction(executions,
                Transactions.TransactionStatus.Pending,
                fiatAssetId).Result;
            transactionRepository.AddEntity(exchangeTransaction).Wait();
            TransactionDetail transactionDetail = exchangeTransaction.TransactionDetail.First();
            Assert.Equal(0, transactionDetail.FiatFee);
            Assert.Equal(10000, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000, transactionDetail.FiatAmountSubtotal);

            Account fiatAccount = transactionRepository.GetAccountByCurrencyId(TestAccounts.User1, fiatAssetId);
            TransactionEntry fiatEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == fiatAccount.AccountId);
            Assert.Equal(10000, fiatEntry.Amount);
            Assert.Equal('B', fiatEntry.TransactionType);

            TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.First(x => x.AccountId != fiatAccount.AccountId);
            Assert.Equal(-1m, cryptoEntry.Amount);
            Assert.Equal('S', cryptoEntry.TransactionType);

            FeeInfo displayFee = new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1);
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(0.015m), TestAccounts.FeeId1),
            };
            executions = new Execution[]
            {
                new Execution()
                {
                    Amount = 1m,
                    Price = 10000,
                    IsBuy = false,
                    Symbol = "BTC",
                    UserId = TestAccounts.User1,
                    OrderId = "test_order_1",
                    TradeId = "test_trade_1",
                    TradeType = TradeType.Limit
                }
            };
            Transaction transaction = transactionRepository.UpdateTradeTransaction(exchangeTransaction.TransactionId, executions.First(), Transactions.TransactionStatus.Completed, displayFee, fees, fiatAssetId).Result;

            transactionDetail = transaction.TransactionDetail.First();
            Assert.Equal(150m, transactionDetail.FiatFee);
            Assert.Equal(9850, transactionDetail.FiatAmountTotal);
            Assert.Equal(10000m, transactionDetail.FiatAmountSubtotal);
            Assert.NotNull(transactionDetail.TransactionEntry.First().TransactionDetailId);

            TransactionDetail feeTransactionDetail1 = transaction.TransactionDetail.First(x => x.TransactionEntry.Any(y => y.AccountId == TestAccounts.FeeAccount1));
            Assert.Equal(150m, feeTransactionDetail1.FiatFee);
            Assert.Equal(9850, feeTransactionDetail1.FiatAmountTotal);
            Assert.Equal(10000m, feeTransactionDetail1.FiatAmountSubtotal);
            Assert.Equal(150m, feeTransactionDetail1.TransactionEntry.First().Amount);
            Assert.NotNull(feeTransactionDetail1.TransactionEntry.First().TransactionDetailId);
        }
    }
}