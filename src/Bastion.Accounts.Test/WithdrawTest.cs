﻿using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;

using System.Linq;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class WithdrawTest : TransactionRepositoryTest
    {
        [Fact]
        public void EmptyFeeTest()
        {
            lock (this)
            {
                var transactionRepository = GetTransactionRepository();
                MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

                Transaction withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 100, null, accountIdTo: TestAccounts.AccountEURUser2).Result;
                var f1 = withdrawTransaction.TransactionDetail.First().FiatFee;
                var f2 = withdrawTransaction.TransactionDetail.First().FiatAmountTotal;
                var f3 = withdrawTransaction.TransactionDetail.First().FiatAmountSubtotal;
                var f4 = withdrawTransaction.TransactionDetail.First().TransactionEntry.First().Amount;

                Assert.Equal(0, f1);
                Assert.Equal(100, f2);
                Assert.Equal(100, f3);
                Assert.Equal(-100, f4);
            }
        }

        [Fact]
        public void FixedFee10Test()
        {
            lock (this)
            {
                var transactionRepository = GetTransactionRepository();
                MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

                Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
                FeeInfo[] fees = new FeeInfo[]
                {
                    new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, account.AccountId, new FixedFee(), TestAccounts.FeeId1)
                };
                Transaction withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 100, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
                TransactionDetail depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
                Assert.Equal(10, depositDetail.FiatFee);
                Assert.Equal(110, depositDetail.FiatAmountTotal);
                Assert.Equal(100, depositDetail.FiatAmountSubtotal);
                Assert.Equal(-110, depositDetail.TransactionEntry.First().Amount);

                withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 11, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
                depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
                Assert.Equal(10, depositDetail.FiatFee);
                Assert.Equal(21, depositDetail.FiatAmountTotal);
                Assert.Equal(11, depositDetail.FiatAmountSubtotal);
                Assert.Equal(-21, depositDetail.TransactionEntry.First().Amount);
            }
        }

        [Fact]
        public void PercentFee1Test()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, account.AccountId, new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 100, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            TransactionDetail depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(1, depositDetail.FiatFee);
            Assert.Equal(101, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);
            Assert.Equal(-101, depositDetail.TransactionEntry.First().Amount);

            withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 11, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(0.11m, depositDetail.FiatFee);
            Assert.Equal(11.11m, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);
            Assert.Equal(-11.11m, depositDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void SimpleComboFee1Test()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, account.AccountId, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, account.AccountId, new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 100, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            TransactionDetail depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(11, depositDetail.FiatFee);
            Assert.Equal(111, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);
            Assert.Equal(-111, depositDetail.TransactionEntry.First().Amount);

            withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 11, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            depositDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10.11m, depositDetail.FiatFee);
            Assert.Equal(21.11m, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);
            Assert.Equal(-21.11m, depositDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void TwoFeeColectorsFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account feeAccoun1 = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            Account feeAccoun2 = transactionRepository.GetAccount(TestAccounts.FeeAccount2).Result;

            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, account.AccountId, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount2, 100) }, account.AccountId, new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 100, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            TransactionDetail withdrawDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(11, withdrawDetail.FiatFee);
            Assert.Equal(111, withdrawDetail.FiatAmountTotal);
            Assert.Equal(100, withdrawDetail.FiatAmountSubtotal);
            Assert.Equal(-111, withdrawDetail.TransactionEntry.First().Amount);

            TransactionDetail firstFeeDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == feeAccoun1.UserId);
            Assert.Equal(10, firstFeeDetail.FiatAmountTotal);
            Assert.Equal(10, firstFeeDetail.FiatAmountSubtotal);
            Assert.Equal(10, firstFeeDetail.TransactionEntry.First().Amount);

            TransactionDetail secondFeeDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == feeAccoun2.UserId);
            Assert.Equal(1, secondFeeDetail.FiatAmountTotal);
            Assert.Equal(1, secondFeeDetail.FiatAmountSubtotal);
            Assert.Equal(1, secondFeeDetail.TransactionEntry.First().Amount);

            withdrawTransaction = transactionRepository.CreateWithdrawTransaction(TestAccounts.AccountEURUser1, 11, fees, accountIdTo: TestAccounts.AccountEURUser2).Result;
            withdrawDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10.11m, withdrawDetail.FiatFee);
            Assert.Equal(21.11m, withdrawDetail.FiatAmountTotal);
            Assert.Equal(11, withdrawDetail.FiatAmountSubtotal);
            Assert.Equal(-21.11m, withdrawDetail.TransactionEntry.First().Amount);

            firstFeeDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == feeAccoun1.UserId);
            Assert.Equal(10, firstFeeDetail.FiatAmountTotal);
            Assert.Equal(10, firstFeeDetail.FiatAmountSubtotal);
            Assert.Equal(10, firstFeeDetail.TransactionEntry.First().Amount);

            secondFeeDetail = withdrawTransaction.TransactionDetail.First(x => x.UserId == feeAccoun2.UserId);
            Assert.Equal(0.11m, secondFeeDetail.FiatAmountTotal);
            Assert.Equal(0.11m, secondFeeDetail.FiatAmountSubtotal);
            Assert.Equal(0.11m, secondFeeDetail.TransactionEntry.First().Amount);

            Assert.Equal(3, withdrawTransaction.TransactionDetail.Count);
        }
    }
}