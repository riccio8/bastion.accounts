﻿using System;
using System.Collections.Generic;
using System.Text;
using Bastion.Accounts.Context;
using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;
using System;
using System.Linq;
using Xunit;
using Bastion.Accounts.Constants;

namespace Bastion.Accounts.Test
{
    public class AcountsTest : TransactionRepositoryTest
    {
        [Fact]
        public void GetAllAccounts()
        {
            var transactionRepository = GetTransactionRepository();

            var acc = transactionRepository.GetAllAccounts().Result;
            int count = acc.Count();
            Assert.Equal(6, count);
            
            foreach (var item in acc)
            {
            
            }
        }

        [Fact]
        public void GetAllAccountsByEur()
        {
            var transactionRepository = GetTransactionRepository();

            var acc = transactionRepository.GetAllAccounts(Assets.EurAssetId).Result;
            int count = acc.Count();
            Assert.Equal(5, count);
        }

        [Fact]
        public void GetAllAccountsByBtc()
        {
            var transactionRepository = GetTransactionRepository();

            var acc = transactionRepository.GetAllAccounts("BTC").Result;
            int count = acc.Count();
            Assert.Equal(1, count);
        }

        [Fact]
        public void GetAllAccountsPerPage()
        {
            var transactionRepository = GetTransactionRepository();

            var acc = transactionRepository.GetAllAccounts(perPage:2).Result;
            int count = acc.Count();
            Assert.Equal(2, count);
        }

        [Fact]
        public void GetAllAccountsDifferentPage()
        {
            var transactionRepository = GetTransactionRepository();

            var acc1page = transactionRepository.GetAllAccounts(page: 1,  perPage: 2).Result;
            var acc2page = transactionRepository.GetAllAccounts(page: 2, perPage: 2).Result;
            Assert.NotEqual(acc1page.First(), acc2page.First());
        }

        [Fact]
        public void GetAllAccountsTypes()
        {
            var transactionRepository = GetTransactionRepository();

            var acc = transactionRepository.GetAllAccounts(page: 1, perPage: 2, accountTypes: "Migom").Result;
            int count = acc.Count();
            Assert.NotEqual(4, count);
        }

    }

}
