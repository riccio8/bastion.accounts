﻿using Bastion.Accounts.Interfaces;

namespace Bastion.Accounts.Test.Fee
{
    public static class FeeInfoConstructor
    {
        public static FeeInfo[] GetFixedFeeInfo(string fromAccountId, decimal value)
        {
            return new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, fromAccountId, new FixedFee(value), TestAccounts.FeeId1)
            };
        }
    }
}