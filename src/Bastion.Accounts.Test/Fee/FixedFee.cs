﻿using Bastion.Accounts.Interfaces;

namespace Bastion.Accounts.Test
{
    public class FixedFee : IFeeCalculate
    {
        private readonly decimal value;

        public FixedFee(decimal value = 10)
        {
            this.value = value;
        }

        public decimal Calculate(decimal amount)
        {
            return this.value;
        }
    }
}