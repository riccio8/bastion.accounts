﻿using Bastion.Accounts.Interfaces;

namespace Bastion.Accounts.Test
{
    public class PercentFee : IFeeCalculate
    {
        private decimal percentFee;

        public PercentFee(decimal percentFee = 0.01m)
        {
            this.percentFee = percentFee;
        }

        public decimal Calculate(decimal amount)
        {
            return amount * percentFee;
        }
    }
}