﻿namespace Bastion.Accounts.Test
{
    public static class TestAccounts
    {
        public const string User1 = "TestUser1";
        public const string User2 = "TestUser2";
        public const string AccountEURUser1 = "TestAccount1";
        public const string AccountEURUser2 = "TestAccount2";
        public const string AccountUser1External = "TestUser1External";
        public const string Account3BTC = "TestAccount3";
        public const string FeeAccount1 = "TestFeeAccount1";
        public const string FeeAccount2 = "TestFeeAccount2";
        public const string FeeId1 = "UNK";
    }
}