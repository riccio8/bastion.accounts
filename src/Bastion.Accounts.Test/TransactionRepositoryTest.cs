﻿using Bastion.Accounts.Context;
using Bastion.Accounts.Models;
using Bastion.Accounts.Stores;

using System;
using System.Linq;

namespace Bastion.Accounts.Test
{
    public class TransactionRepositoryTest
    {
        private static Random random = new Random();
        private static AccountsStore accountsStore = new AccountsStore();
        private static CurrencyStore currencyStore = new CurrencyStore();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public TransactionRepository GetTransactionRepository(string dbName = null)
        {
            if (dbName == null)
                dbName = RandomString(10); // если не передали имя тестовой базы, сгенерируем случайное имя

            var transactionRepository = new TransactionRepository(accountsStore, currencyStore, new AccountContext(dbName));
            transactionRepository.AddEntities(AccountContext.GetTransactionStateData()).Wait();
            transactionRepository.AddEntities(AccountContext.GetTransactionTypeData()).Wait();
            transactionRepository.AddEntities(GetTestCurrency()).Wait();
            transactionRepository.AddEntities(GetTestAccounts()).Wait();
            return transactionRepository;
        }

        protected void MakeDeposit(TransactionRepository transactionRepository, string accountId, decimal amount)
        {
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(accountId, amount, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
        }

        private static Currency[] GetTestCurrency()
        {
            return new Currency[]
            {
                new Currency()
                {
                    CurrencyId = "EUR",
                    DecimalPoints = 2,
                    Formatter = "N2"
                },
                new Currency()
                {
                    CurrencyId = "BTC",
                    DecimalPoints = 8,
                    Formatter = "N8"
                },
                new Currency()
                {
                    CurrencyId = "USD",
                    DecimalPoints = 2,
                    Formatter = "N2"
                },
            };
        }

        private static Account[] GetTestAccounts()
        {
            return new Account[]
            {
                new Account()
                {
                    AccountId = "TestAccount1",
                    CurrencyId = "EUR",
                    UserId = "TestUser1",
                    AccountNumber = "TestAccountNumber1",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestAccount2",
                    CurrencyId = "EUR",
                    UserId = "TestUser2",
                    AccountNumber = "TestAccountNumber2",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestAccount3",
                    CurrencyId = "BTC",
                    UserId = "TestUser1",
                    AccountNumber = "TestAccountNumber3",
                    AccountTypeId = "DIMM"
                },
                new Account()
                {
                    AccountId = "TestUser1External",
                    CurrencyId = "EUR",
                    UserId = "TestUser1",
                    AccountNumber = "ExternalAccount",
                    AccountTypeId = "External"
                },
                new Account()
                {
                    AccountId = "TestFeeAccount1",
                    CurrencyId = "EUR",
                    UserId = "TestFeeUser1",
                    AccountNumber = "TestFeeAccountNumber1",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestFeeAccount2",
                    CurrencyId = "EUR",
                    UserId = "TestFeeUser2",
                    AccountNumber = "TestFeeAccountNumber2",
                    AccountTypeId = "Migom"
                },
            };
        }
    }
}