﻿using Bastion.Accounts.Models;
using Bastion.Accounts.Test.Fee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Bastion.Accounts.Test
{
    public class TransferTest : TransactionRepositoryTest
    {
        [Fact]
        public void EmptyFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountUser1External).Result;
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 200);
            Transaction depositTransaction = transactionRepository.CreateTransferTransaction(fromAccount.AccountId, toAccount.AccountId, 100, null).Result;

            TransactionDetail detail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(0, detail.FiatFee);
            Assert.Equal(100, detail.FiatAmountTotal);
            Assert.Equal(100, detail.FiatAmountSubtotal);
            Assert.Equal(-100, detail.TransactionEntry.First(x => x.AccountId == TestAccounts.AccountEURUser1).Amount);
            Assert.Equal(100, detail.TransactionEntry.First(x => x.AccountId == TestAccounts.AccountUser1External).Amount);
        }

        [Fact]
        public void FixedFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountUser1External).Result;
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 200);
            var fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 10);
            Transaction depositTransaction = transactionRepository.CreateTransferTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;

            TransactionDetail detail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, detail.FiatFee);
            Assert.Equal(110, detail.FiatAmountTotal);
            Assert.Equal(100, detail.FiatAmountSubtotal);
            Assert.Equal(-110, detail.TransactionEntry.First(x => x.AccountId == TestAccounts.AccountEURUser1).Amount);
            Assert.Equal(100, detail.TransactionEntry.First(x => x.AccountId == TestAccounts.AccountUser1External).Amount);
        }

        [Fact]
        public void FixedFeeNewBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountUser1External).Result;
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 200);
            var fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 10);
            Transaction depositTransaction = transactionRepository.CreateTransferTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;

            TransactionDetail detail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(90, detail.TransactionEntry.First(x => x.AccountId == TestAccounts.AccountEURUser1).NewBalance);
        }
    }
}
