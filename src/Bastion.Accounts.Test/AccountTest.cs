﻿using Bastion.Accounts.Models;

using System;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class AccountTest : TransactionRepositoryTest
    {
        public AccountTest()
        {
        }

        [Fact]
        public void GetAccountTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Assert.NotNull(account);
            Assert.NotNull(account.Currency);
        }

        [Fact]
        public void GetAccountEmptyTest()
        {
            var transactionRepository = GetTransactionRepository();
            Assert.Throws<AggregateException>(() => transactionRepository.GetAccount(string.Empty).Result);
        }

        [Fact]
        public void GetUnknownAccountTest()
        {
            var transactionRepository = GetTransactionRepository();
            Assert.Throws<AggregateException>(() => transactionRepository.GetAccount("XYZ").Result);
        }
    }
}