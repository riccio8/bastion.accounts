﻿using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;

using System.Linq;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class SendTest : TransactionRepositoryTest
    {
        public SendTest()
        {
            //AccountContext.IsTestEnvironment = true;
        }

        [Fact]
        public void EmptyFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser2).Result;
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 200);
            Transaction depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 100, null).Result;

            TransactionDetail fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(0, fromDetail.FiatFee);
            Assert.Equal(100, fromDetail.FiatAmountTotal);
            Assert.Equal(100, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-100, fromDetail.TransactionEntry.First().Amount);

            TransactionDetail toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(0, toDetail.FiatFee);
            Assert.Equal(100, toDetail.FiatAmountTotal);
            Assert.Equal(100, toDetail.FiatAmountSubtotal);
            Assert.Equal(100, toDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void FixedFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser2).Result;
            Account feeAccount = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, fromAccount.AccountId, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, toAccount.AccountId, new FixedFee(), TestAccounts.FeeId1),
            };

            Transaction depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;
            TransactionDetail fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(110, fromDetail.FiatAmountTotal);
            Assert.Equal(100, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-110, fromDetail.TransactionEntry.First().Amount);

            TransactionDetail toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(10, toDetail.FiatFee);
            Assert.Equal(90, toDetail.FiatAmountTotal);
            Assert.Equal(100, toDetail.FiatAmountSubtotal);
            Assert.Equal(90, toDetail.TransactionEntry.First().Amount);

            TransactionDetail feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(20, feeDetail.FiatAmountTotal);
            Assert.Equal(20, feeDetail.FiatAmountSubtotal);
            Assert.Equal(20, feeDetail.TransactionEntry.First().Amount);

            depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 200, fees).Result;
            fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(210, fromDetail.FiatAmountTotal);
            Assert.Equal(200, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-210, fromDetail.TransactionEntry.First().Amount);

            toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(10, toDetail.FiatFee);
            Assert.Equal(190, toDetail.FiatAmountTotal);
            Assert.Equal(200, toDetail.FiatAmountSubtotal);
            Assert.Equal(190, toDetail.TransactionEntry.First().Amount);

            feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(20, feeDetail.FiatAmountTotal);
            Assert.Equal(20, feeDetail.FiatAmountSubtotal);
            Assert.Equal(20, feeDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void PercentFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser2).Result;
            Account feeAccount = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, fromAccount.AccountId, new PercentFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, toAccount.AccountId, new PercentFee(), TestAccounts.FeeId1),
            };

            Transaction depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;
            TransactionDetail fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(1, fromDetail.FiatFee);
            Assert.Equal(101, fromDetail.FiatAmountTotal);
            Assert.Equal(100, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-101, fromDetail.TransactionEntry.First().Amount);

            TransactionDetail toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(1, toDetail.FiatFee);
            Assert.Equal(99, toDetail.FiatAmountTotal);
            Assert.Equal(100, toDetail.FiatAmountSubtotal);
            Assert.Equal(99, toDetail.TransactionEntry.First().Amount);

            TransactionDetail feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(2, feeDetail.FiatAmountTotal);
            Assert.Equal(2, feeDetail.FiatAmountSubtotal);
            Assert.Equal(2, feeDetail.TransactionEntry.First().Amount);

            depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 200, fees).Result;
            fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(2, fromDetail.FiatFee);
            Assert.Equal(202, fromDetail.FiatAmountTotal);
            Assert.Equal(200, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-202, fromDetail.TransactionEntry.First().Amount);

            toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(2, toDetail.FiatFee);
            Assert.Equal(198, toDetail.FiatAmountTotal);
            Assert.Equal(200, toDetail.FiatAmountSubtotal);
            Assert.Equal(198, toDetail.TransactionEntry.First().Amount);

            feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(4, feeDetail.FiatAmountTotal);
            Assert.Equal(4, feeDetail.FiatAmountSubtotal);
            Assert.Equal(4, feeDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void DifferentFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser2).Result;
            Account feeAccount = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, fromAccount.AccountId, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, toAccount.AccountId, new PercentFee(), TestAccounts.FeeId1),
            };

            Transaction depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;
            TransactionDetail fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(110, fromDetail.FiatAmountTotal);
            Assert.Equal(100, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-110, fromDetail.TransactionEntry.First().Amount);

            TransactionDetail toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(1, toDetail.FiatFee);
            Assert.Equal(99, toDetail.FiatAmountTotal);
            Assert.Equal(100, toDetail.FiatAmountSubtotal);
            Assert.Equal(99, toDetail.TransactionEntry.First().Amount);

            TransactionDetail feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(11, feeDetail.FiatAmountTotal);
            Assert.Equal(11, feeDetail.FiatAmountSubtotal);
            Assert.Equal(11, feeDetail.TransactionEntry.First().Amount);

            depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 200, fees).Result;
            fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(210, fromDetail.FiatAmountTotal);
            Assert.Equal(200, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-210, fromDetail.TransactionEntry.First().Amount);

            toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(2, toDetail.FiatFee);
            Assert.Equal(198, toDetail.FiatAmountTotal);
            Assert.Equal(200, toDetail.FiatAmountSubtotal);
            Assert.Equal(198, toDetail.TransactionEntry.First().Amount);

            feeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount.UserId);
            Assert.Equal(0, feeDetail.FiatFee);
            Assert.Equal(12, feeDetail.FiatAmountTotal);
            Assert.Equal(12, feeDetail.FiatAmountSubtotal);
            Assert.Equal(12, feeDetail.TransactionEntry.First().Amount);
        }

        [Fact]
        public void DifferentFeeCollectorsTest()
        {
            var transactionRepository = GetTransactionRepository();
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 1000);

            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser2).Result;
            Account feeAccount1 = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            Account feeAccount2 = transactionRepository.GetAccount(TestAccounts.FeeAccount2).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, fromAccount.AccountId, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount2, 100) }, toAccount.AccountId, new PercentFee(), TestAccounts.FeeId1),
            };

            Transaction depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;
            TransactionDetail fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(110, fromDetail.FiatAmountTotal);
            Assert.Equal(100, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-110, fromDetail.TransactionEntry.First().Amount);

            TransactionDetail toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(1, toDetail.FiatFee);
            Assert.Equal(99, toDetail.FiatAmountTotal);
            Assert.Equal(100, toDetail.FiatAmountSubtotal);
            Assert.Equal(99, toDetail.TransactionEntry.First().Amount);

            TransactionDetail feeDetail1 = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount1.UserId);
            Assert.Equal(0, feeDetail1.FiatFee);
            Assert.Equal(10, feeDetail1.FiatAmountTotal);
            Assert.Equal(10, feeDetail1.FiatAmountSubtotal);
            Assert.Equal(10, feeDetail1.TransactionEntry.First().Amount);

            TransactionDetail feeDetail2 = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount2.UserId);
            Assert.Equal(0, feeDetail2.FiatFee);
            Assert.Equal(1, feeDetail2.FiatAmountTotal);
            Assert.Equal(1, feeDetail2.FiatAmountSubtotal);
            Assert.Equal(1, feeDetail2.TransactionEntry.First().Amount);

            depositTransaction = transactionRepository.CreateSendTransaction(fromAccount.AccountId, toAccount.AccountId, 200, fees).Result;
            fromDetail = depositTransaction.TransactionDetail.First(x => x.UserId == fromAccount.UserId);
            Assert.Equal(10, fromDetail.FiatFee);
            Assert.Equal(210, fromDetail.FiatAmountTotal);
            Assert.Equal(200, fromDetail.FiatAmountSubtotal);
            Assert.Equal(-210, fromDetail.TransactionEntry.First().Amount);

            toDetail = depositTransaction.TransactionDetail.First(x => x.UserId == toAccount.UserId);
            Assert.Equal(2, toDetail.FiatFee);
            Assert.Equal(198, toDetail.FiatAmountTotal);
            Assert.Equal(200, toDetail.FiatAmountSubtotal);
            Assert.Equal(198, toDetail.TransactionEntry.First().Amount);

            feeDetail1 = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount1.UserId);
            Assert.Equal(0, feeDetail1.FiatFee);
            Assert.Equal(10, feeDetail1.FiatAmountTotal);
            Assert.Equal(10, feeDetail1.FiatAmountSubtotal);
            Assert.Equal(10, feeDetail1.TransactionEntry.First().Amount);

            feeDetail2 = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccount2.UserId);
            Assert.Equal(0, feeDetail1.FiatFee);
            Assert.Equal(2, feeDetail2.FiatAmountTotal);
            Assert.Equal(2, feeDetail2.FiatAmountSubtotal);
            Assert.Equal(2, feeDetail2.TransactionEntry.First().Amount);
        }
    }
}