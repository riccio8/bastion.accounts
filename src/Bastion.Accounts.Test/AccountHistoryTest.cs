﻿using Bastion.Accounts.Constants;
using Bastion.Accounts.ExtraModels;
using Bastion.Accounts.Helpers;
using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;
using Bastion.Accounts.Test.Fee;

using System;
using System.Linq;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class AccountHistoryTest : TransactionRepositoryTest
    {
        public AccountHistoryTest()
        {
            //  AccountContext.IsTestEnvironment = true;
        }

        [Fact]
        public void AddDepositTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            ShortTransactionReport report = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.First();
            Assert.Equal(1000, report.AmountValue);
            Assert.Equal(1000, report.TotalValue);
        }

        [Fact]
        public void AddDepositAndFixedFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            FeeInfo[] fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 15);
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, fees).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            ShortTransactionReport report = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.First();
            Assert.Equal(985, report.AmountValue);
            Assert.Equal(985, report.TotalValue);
        }

        [Fact]
        public void AddDepositAndWithdrawFixedFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();

            FeeInfo[] fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 15);
            var withdrawTransaction = transactionRepository.CreateSimpleWithdrawTransaction(TestAccounts.AccountEURUser1, 1000, fees).Result;
            transactionRepository.AddEntity(withdrawTransaction).Wait();

            ShortTransactionReport report = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.Last();
            Assert.Equal(-1015, report.AmountValue);
            Assert.Equal(-1015, report.TotalValue);
        }

        [Fact]
        public void Add3DepositCountTest()
        {
            var transactionRepository = GetTransactionRepository();
            var d1 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            var d2 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 2000, null).Result;
            var d3 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 3000, null).Result;
            transactionRepository.AddEntities(new Transaction[] { d1, d2, d3 }).Wait();
            var reportItems = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result;
            int c = reportItems.Count();
            Assert.Equal(3, c);
        }

        [Fact]
        public void Add3Deposit1WithdrawCountTest()
        {
            var transactionRepository = GetTransactionRepository();
            var d1 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            var d2 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 2000, null).Result;
            var d3 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 3000, null).Result;
            transactionRepository.AddEntities(new Transaction[] { d1, d2, d3 }).Wait();

            Transaction w1 = transactionRepository.CreateSimpleWithdrawTransaction(TestAccounts.AccountEURUser1, 100, null).Result;
            transactionRepository.AddEntities(new Transaction[] { w1 }).Wait();

            var reportItems = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result;
            int c = reportItems.Count();
            Assert.Equal(4, c);
            int depositCount = reportItems.Where(x => x.Type == TransactionTypeConverter.GetString(TransactionTypeChar.Deposit)).Count();
            Assert.Equal(3, depositCount);
            int withdrawCount = reportItems.Where(x => x.Type == TransactionTypeConverter.GetString(TransactionTypeChar.Withdraw)).Count();
            Assert.Equal(1, withdrawCount);
        }

        //[Fact]
        //public void DepositExchangeTest()
        //{
        //    var transactionRepository = GetTransactionRepository();
        //    var d1 = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
        //    transactionRepository.AddEntity(d1).Wait();

        //    Transaction w1 = transactionRepository.CreateSimpleWithdrawTransaction(TestAccounts.AccountEURUser1, 100, null).Result;
        //    transactionRepository.AddEntities(new Transaction[] { w1 }).Wait();

        //    Execution[] executions = new Execution[]
        //    {
        //        new Execution()
        //        {
        //            Amount = 0.1m,
        //            IsBuy = true,
        //            OrderId = "test1",
        //            Price = 9000,
        //            Symbol = "BTC",
        //            UserId = TestAccounts.User1,
        //            TradeId = "test1",
        //            TradeType = TradeType.FixCoin,
        //            Ts = DateTime.UtcNow
        //        }
        //    };
        //    var tradeTransaction = transactionRepository.CreateTradeTransaction(executions, assetId: Assets.EurAssetId).Result;
        //    transactionRepository.AddEntity(tradeTransaction).Wait();

        //    var reportEur = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.Last();
        //    Assert.Equal(-900, reportEur.AmountValue);
        //    Assert.Equal(900, reportEur.TotalValue);
        //    Assets.Equals(TransactionTypeChar.Exchange, reportEur.TransactionType);

        //    var reportBtc = transactionRepository.GetNewTransactionReport(TestAccounts.Account3BTC).Result.Last();
        //    Assert.Equal(0.1m, reportBtc.AmountValue);
        //    Assert.Equal(900, reportBtc.TotalValue);
        //    Assets.Equals(TransactionTypeChar.Exchange, reportBtc.TransactionType);
        //}

        [Fact]
        public void SendSearchToUserTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            var sendTransaction = transactionRepository.CreateSendTransaction(TestAccounts.AccountEURUser1, TestAccounts.AccountEURUser2, 490, null).Result;
            transactionRepository.AddEntity(sendTransaction).Wait();
            var reports = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1, toUserId: TestAccounts.User2).Result;
            Assert.Single(reports);
        }

        [Fact]
        public void SendSearchFromUserTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            var sendTransaction = transactionRepository.CreateSendTransaction(TestAccounts.AccountEURUser1, TestAccounts.AccountEURUser2, 490, null).Result;
            transactionRepository.AddEntity(sendTransaction).Wait();
            var reports = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser2, fromUserId: TestAccounts.User1).Result;
            Assert.Single(reports);
        }

        [Fact]
        public void SendToOtherUserTest()
        {
            var transactionRepository = GetTransactionRepository();

            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();

            var sendTransaction = transactionRepository.CreateSendTransaction(TestAccounts.AccountEURUser1, TestAccounts.AccountEURUser2, 666, null).Result;
            transactionRepository.AddEntity(sendTransaction).Wait();

            var reportUser1 = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.Last();
            Assert.Equal(-666, reportUser1.AmountValue);
            Assert.Equal(-666, reportUser1.TotalValue);
            Assert.Equal(TransactionTypeConverter.GetString(TransactionTypeChar.Send), reportUser1.Type);

            var reportUser2 = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser2).Result.Last();
            Assert.Equal(666, reportUser2.AmountValue);
            Assert.Equal(666, reportUser2.TotalValue);
            Assert.Equal(TransactionTypeConverter.GetString(TransactionTypeChar.Receive), reportUser2.Type);
        }

        [Fact]
        public void TransferFixedFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account fromAccount = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account toAccount = transactionRepository.GetAccount(TestAccounts.AccountUser1External).Result;
            MakeDeposit(transactionRepository, TestAccounts.AccountEURUser1, 200);
            var fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 10);
            Transaction transferTransaction = transactionRepository.CreateTransferTransaction(fromAccount.AccountId, toAccount.AccountId, 100, fees).Result;

            transactionRepository.AddEntity(transferTransaction).Wait();

            var report = transactionRepository.GetNewTransactionReport(TestAccounts.AccountEURUser1).Result.Last();

            Assert.Equal(-110, report.AmountValue);
            Assert.Equal(90, report.NewBalanceValue);
            Assert.Equal(-110, report.TotalValue);
        }
    }
}