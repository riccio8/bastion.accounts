﻿using Bastion.Accounts.Constants;
using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;
using Bastion.Accounts.Test.Fee;

using System;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class BalanceTest : TransactionRepositoryTest
    {
        public BalanceTest()
        {
        }

        [Fact]
        public void EmptyBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            decimal balance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(0, balance);
        }

        [Fact]
        public void DepositBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(1000, balance);
        }

        [Fact]
        public void PendingDepositBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null, Transactions.TransactionStatus.Pending).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(0, balance);
        }

        [Fact]
        public void DepositWithdrawBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            var withdrawTransaction = transactionRepository.CreateSimpleWithdrawTransaction(TestAccounts.AccountEURUser1, 900, null).Result;
            transactionRepository.AddEntity(withdrawTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(100, balance);
        }

        [Fact]
        public void DepositPendingWithdrawBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            var withdrawTransaction = transactionRepository.CreateSimpleWithdrawTransaction(TestAccounts.AccountEURUser1, 900, null, Transactions.TransactionStatus.Pending).Result;
            transactionRepository.AddEntity(withdrawTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(100, balance);
        }

        [Fact]
        public void CryptoDepositBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateDepositCryptoTransaction(TestAccounts.Account3BTC, 0.2m, 10000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.Account3BTC).Result;
            Assert.Equal(0.2m, balance);
        }

        [Fact]
        public void CryptoDepositWithdrawBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            var depositTransaction = transactionRepository.CreateDepositCryptoTransaction(TestAccounts.Account3BTC, 0.2m, 10000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            var withdrawTransaction = transactionRepository.CreateWithdrawCryptoTransaction(TestAccounts.Account3BTC, 0.05m, 10000, null).Result;
            transactionRepository.AddEntity(withdrawTransaction).Wait();
            decimal balance = transactionRepository.GetBalance(TestAccounts.Account3BTC).Result;
            Assert.Equal(0.15m, balance);
        }

        [Fact]
        public void CryptoDepositFixFeeBalanceTest()
        {
            var transactionRepository = GetTransactionRepository();
            FeeInfo[] fees = FeeInfoConstructor.GetFixedFeeInfo(TestAccounts.AccountEURUser1, 15);
            var depositTransaction = transactionRepository.CreateDepositCryptoTransaction(TestAccounts.Account3BTC, 0.2m, 10000, fees).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();
            decimal btcBalance = transactionRepository.GetBalance(TestAccounts.Account3BTC).Result;
            Assert.Equal(0.2m, btcBalance);
            decimal eurBalance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(-15, eurBalance);
        }

        //[Fact]
        //public void ExcahngeBalanceTest()
        //{
        //    var transactionRepository = GetTransactionRepository();

        //    var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
        //    transactionRepository.AddEntity(depositTransaction).Wait();

        //    Execution[] executions = new Execution[]
        //    {
        //        new Execution()
        //        {
        //            Amount = 0.1m,
        //            IsBuy = true,
        //            OrderId = "test1",
        //            Price = 9000,
        //            Symbol = "BTC",
        //            UserId = TestAccounts.User1,
        //            TradeId = "test1",
        //            TradeType = TradeType.FixCoin,
        //            Ts = DateTime.UtcNow
        //        }
        //    };
        //    var tradeTransaction = transactionRepository.CreateTradeTransaction(executions, assetId: Assets.EurAssetId).Result;
        //    transactionRepository.AddEntity(tradeTransaction).Wait();
        //    decimal btcBalance = transactionRepository.GetBalance(TestAccounts.Account3BTC).Result;
        //    Assert.Equal(0.1m, btcBalance);
        //    decimal eurBalance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
        //    Assert.Equal(100, eurBalance);
        //}

        [Fact]
        public void SendToOtherAccountTest()
        {
            var transactionRepository = GetTransactionRepository();

            var depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            transactionRepository.AddEntity(depositTransaction).Wait();

            var sendTransaction = transactionRepository.CreateSendTransaction(TestAccounts.AccountEURUser1, TestAccounts.AccountEURUser2, 666, null).Result;
            transactionRepository.AddEntity(sendTransaction).Wait();

            decimal btcBalance = transactionRepository.GetBalance(TestAccounts.AccountEURUser1).Result;
            Assert.Equal(334, btcBalance);
            decimal eurBalance = transactionRepository.GetBalance(TestAccounts.AccountEURUser2).Result;
            Assert.Equal(666, eurBalance);
        }
    }
}