﻿using Bastion.Accounts.Constants;
using Bastion.Accounts.Models;

using System;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class CurrencyTest : TransactionRepositoryTest
    {
        public CurrencyTest()
        {
        }

        [Fact]
        public void GetCurrencyTest()
        {
            var transactionRepository = GetTransactionRepository();
            Currency currency = transactionRepository.GetCurrency(Assets.EurAssetId).Result;
            Assert.NotNull(currency);
        }

        [Fact]
        public void GetCurrencyEmptyTest()
        {
            var transactionRepository = GetTransactionRepository();
            Assert.Throws<AggregateException>(() => transactionRepository.GetCurrency(string.Empty).Result);
        }

        [Fact]
        public void GetCurrencyAccountTest()
        {
            var transactionRepository = GetTransactionRepository();
            Assert.Throws<AggregateException>(() => transactionRepository.GetCurrency("XYZ").Result);
        }
    }
}