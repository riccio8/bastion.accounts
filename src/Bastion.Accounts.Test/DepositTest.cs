using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;

using System.Linq;

using Xunit;

namespace Bastion.Accounts.Test
{
    public class DepositTest : TransactionRepositoryTest
    {
        public DepositTest()
        {
            //  AccountContext.IsTestEnvironment = true;
        }

        [Fact]
        public void EmptyFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 1000, null).Result;
            Assert.Equal(0, depositTransaction.TransactionDetail.First().FiatFee);
            Assert.Equal(1000, depositTransaction.TransactionDetail.First().FiatAmountTotal);
            Assert.Equal(1000, depositTransaction.TransactionDetail.First().FiatAmountSubtotal);
        }

        [Fact]
        public void FixedFee10Test()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new FixedFee(), TestAccounts.FeeId1)
            };
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 100, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10, depositDetail.FiatFee);
            Assert.Equal(90, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);

            depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 11, fees).Result;
            depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10, depositDetail.FiatFee);
            Assert.Equal(1, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);
        }

        [Fact]
        public void PercentFee1Test()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1,
                new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 100, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(1, depositDetail.FiatFee);
            Assert.Equal(99, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);

            depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 11, fees).Result;
            depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(0.11m, depositDetail.FiatFee);
            Assert.Equal(10.89m, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);
        }

        [Fact]
        public void SimpleComboFee1Test()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new FixedFee(), TestAccounts.FeeId1),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 100, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(11, depositDetail.FiatFee);
            Assert.Equal(89, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);

            depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 11, fees).Result;
            depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10.11m, depositDetail.FiatFee);
            Assert.Equal(0.89m, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);
        }

        [Fact]
        public void TwoFeeColectorsFeeTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            Account feeAccoun1 = transactionRepository.GetAccount(TestAccounts.FeeAccount1).Result;
            Account feeAccoun2 = transactionRepository.GetAccount(TestAccounts.FeeAccount2).Result;

            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new FixedFee(), account.UserId),
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount2, 100) }, TestAccounts.AccountEURUser1, new PercentFee(), account.UserId)
            };
            Transaction depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 100, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(11, depositDetail.FiatFee);
            Assert.Equal(89, depositDetail.FiatAmountTotal);
            Assert.Equal(100, depositDetail.FiatAmountSubtotal);

            TransactionDetail firstFeeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccoun1.UserId);
            Assert.Equal(10, firstFeeDetail.FiatAmountTotal);
            Assert.Equal(10, firstFeeDetail.FiatAmountSubtotal);
            Assert.Equal(10, firstFeeDetail.TransactionEntry.First().Amount);

            TransactionDetail secondFeeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccoun2.UserId);
            Assert.Equal(1, secondFeeDetail.FiatAmountTotal);
            Assert.Equal(1, secondFeeDetail.FiatAmountSubtotal);
            Assert.Equal(1, secondFeeDetail.TransactionEntry.First().Amount);

            depositTransaction = transactionRepository.CreateSimpleDepositTransaction(TestAccounts.AccountEURUser1, 11, fees).Result;
            depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(10.11m, depositDetail.FiatFee);
            Assert.Equal(0.89m, depositDetail.FiatAmountTotal);
            Assert.Equal(11, depositDetail.FiatAmountSubtotal);

            firstFeeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccoun1.UserId);
            Assert.Equal(10, firstFeeDetail.FiatAmountTotal);
            Assert.Equal(10, firstFeeDetail.FiatAmountSubtotal);
            Assert.Equal(10, firstFeeDetail.TransactionEntry.First().Amount);

            secondFeeDetail = depositTransaction.TransactionDetail.First(x => x.UserId == feeAccoun2.UserId);
            Assert.Equal(0.11m, secondFeeDetail.FiatAmountTotal);
            Assert.Equal(0.11m, secondFeeDetail.FiatAmountSubtotal);
            Assert.Equal(0.11m, secondFeeDetail.TransactionEntry.First().Amount);

            Assert.Equal(3, depositTransaction.TransactionDetail.Count);
        }

        [Fact]
        public void CryptoDepositTest()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(), TestAccounts.FeeId1)
            };
            Transaction depositTransaction = transactionRepository.CreateDepositCryptoTransaction(TestAccounts.Account3BTC, 10, 7000, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(700, depositDetail.FiatFee);
            Assert.Equal(69300, depositDetail.FiatAmountTotal);
            Assert.Equal(70000, depositDetail.FiatAmountSubtotal);
        }

        [Fact]
        public void CryptoDepositTest2()
        {
            var transactionRepository = GetTransactionRepository();
            Account account = transactionRepository.GetAccount(TestAccounts.AccountEURUser1).Result;
            FeeInfo[] fees = new FeeInfo[]
            {
                new FeeInfo(new CollectorInfo[] { new CollectorInfo(TestAccounts.FeeAccount1, 100) }, TestAccounts.AccountEURUser1, new PercentFee(), TestAccounts.FeeId1)
            };

            Transaction depositTransaction = transactionRepository.CreateDepositCryptoTransaction(TestAccounts.Account3BTC, 1, 7000, fees).Result;
            TransactionDetail depositDetail = depositTransaction.TransactionDetail.First(x => x.UserId == account.UserId);
            Assert.Equal(70, depositDetail.FiatFee);
            Assert.Equal(6930, depositDetail.FiatAmountTotal);
            Assert.Equal(7000, depositDetail.FiatAmountSubtotal);
        }
    }
}