# Versions

### 1.0.3.3
Create transaction allow transactions without fiat currency and without price. 

### 1.0.3.2
Add locks to BalanceStore

### 1.0.3.1
Can specify list of commissions in CreateTradeTransaction, then for each commission separate TransactionDetail is added inside that Transaction.