﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Models;
    using Bastion.PubSub;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public partial class TransactionRepository
    {
        public async override Task<T> AddEntity<T>(T obj)
        {
            T entity = await base.AddEntity(obj);
            if (typeof(T) == typeof(Transaction))
            {
                //await balanceStore.UpdateBalance(entity as Transaction);
                this.Publish(entity as Transaction);
            }
            this.context.Entry<T>(obj).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            return entity;
        }

        public async override Task<IEnumerable<T>> AddEntities<T>(IEnumerable<T> objects)
            //where T : Transaction
        {
            IEnumerable<T> entities = await base.AddEntities(objects);
            if (typeof(T) == typeof(Transaction))
            {
                foreach (var entity in entities)
                {
                    //await balanceStore.UpdateBalance(entity as Transaction);
                    this.Publish(entity as Transaction);
                }
            }
            return entities;
        }

        public async override Task UpdateEntity(object obj)
        {
            await base.UpdateEntity(obj);
            if (obj is Transaction transaction)
            {
                //await balanceStore.UpdateBalance(transaction);
                this.Publish(obj as Transaction);
            }
        }

        public async override Task<IEnumerable<T>> UpdateEntities<T>(IEnumerable<T> objects)
        {
            IEnumerable<T> entities = await base.UpdateEntities(objects);
            if (typeof(T) == typeof(Transaction))
            {
                foreach (var entity in entities)
                {
                    //await balanceStore.UpdateBalance(entity as Transaction);
                    this.Publish(entity as Transaction);
                }
            }
            return entities;
        }
    }
}
