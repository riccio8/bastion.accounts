﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Constants;
    using Bastion.Accounts.Context;
    using Bastion.Accounts.Extensions;
    using Bastion.Accounts.Interfaces;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Transactions;
    using Bastion.Helper;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public partial class TransactionRepository
    {
        private object lockObj = new object();

        private async Task<Transaction> CreateTransaction(string description, 
            char type, 
            TransactionStatus state,
            IEnumerable<TransactionDetail> details, 
            IEnumerable<FeeInfo> fees, 
            TransactionInfo transactionInfo = null)
        {
            Transaction transaction = new Transaction(description, (char)state, type, transactionInfo);
            foreach (var detail in details)
            {
                detail.TransactionId = transaction.TransactionId;
                detail.TransactionDetailId = transaction.GetNextDetailId();

                bool isTotalFilled = false;
                foreach (TransactionEntry entry in detail.TransactionEntry)
                {
                    decimal accountBalance = await this.GetBalance(entry.AccountId);
                    entry.PrevBalance = accountBalance;
                    entry.TransactionDetailId = detail.TransactionDetailId;
                    
                    Account account = await this.GetAccount(entry.AccountId);
                    entry.Amount = account.ExtraRound(entry.Amount);
                    if (!isTotalFilled)
                    {
                        if (Assets.IsFiatAsset(account.CurrencyId))
                        {
                            this.FilledTotal(fees, entry.Amount, detail);
                            isTotalFilled = true;
                        }
                        else
                        {
                            if (detail.Price != null)
                            {
                                this.FilledTotal(fees, entry.Amount * detail.Price.Value, detail);
                                isTotalFilled = true;
                            }
                        }
                    }
                    entry.NewBalance = state == TransactionStatus.Completed ? accountBalance + entry.Amount : accountBalance;
                    if (entry.NewBalance < 0)
                    {
                        throw new Exception(
                            $"Not enough balance on account {entry.AccountId}. Current balance: {accountBalance} {account.CurrencyId}");
                    }

                    detail.TransactionEntry.Add(entry);
                }

                if (!isTotalFilled)
                {
                    throw new Exception("Can't calculate total.");
                }
                transaction.TransactionDetail.Add(detail);
                if (fees != null)
                {
                    await this.FilledFees(fees, detail, transaction);
                }
            }

            transaction.TotalFee = transaction.TransactionDetail.Sum(x => x.FiatFee);
            return transaction;
        }

        private async Task<Transaction> CreateTransaction(string description, 
            char type, TransactionStatus state,
            TransactionDetail detail, 
            IEnumerable<FeeInfo> fees, 
            TransactionInfo transactionInfo = null)
        {
            return await CreateTransaction(description,
                type,
                state,
                new List<TransactionDetail>() { detail },
                fees,
                transactionInfo);
        }

        private async Task FilledFees(IEnumerable<FeeInfo> fees, TransactionDetail userDetail, Transaction transaction)
        {
            if (fees == null)
            {
                transaction.FeeId = "UNK";
                return;
            }
            if (fees.Count() == 0)
            {
                transaction.FeeId = "UNK";
            }
            else
            {
                transaction.FeeId = fees.First().FeeId;
            }
            foreach (var fee in fees)
            {
                Account userFeeAccount = await this.GetAccount(fee.TragetAccounId);
                if (userDetail.UserId != userFeeAccount.UserId)
                {
                    continue;
                }
                decimal feeValueTotal = fee.FeeCalculate.Calculate((decimal)userDetail.FiatAmountSubtotal);
                foreach (CollectorInfo collector in fee.Collectors)
                {
                    Account feeAccount = await this.GetAccount(collector.AccountId);
                    CustomValidator.CheckNotNull(feeAccount, $"Fee account '{collector.AccountId}' not exist.");
                    decimal feeValue = feeAccount.ExtraRound(feeValueTotal * collector.Value / 100m);
                    TransactionDetail feeDetail = transaction.TransactionDetail.FirstOrDefault(x => x.UserId == feeAccount.UserId);
                    if (feeDetail == null)
                    {
                        feeDetail = new TransactionDetail(collector.Comment ?? "Fee", feeAccount.UserId, userDetail.OrderId, userDetail.TradeId, userDetail.Price);
                        feeDetail.TransactionId = transaction.TransactionId;
                        feeDetail.TransactionDetailId = transaction.GetNextDetailId();
                        feeDetail.FiatAmountSubtotal = feeValue;
                        feeDetail.FiatAmountTotal = feeValue;
                        decimal feeAccountBalance = await this.GetBalance(feeAccount.AccountId);
                        feeDetail.AddEntry(new TransactionEntry(feeAccount.AccountId, feeValue, 'F', feeAccountBalance, feeAccountBalance + feeValue));
                        transaction.TransactionDetail.Add(feeDetail);
                    }
                    else
                    {
                        feeDetail.FiatAmountSubtotal += feeValue;
                        feeDetail.FiatAmountTotal += feeValue;
                        TransactionEntry feeEntry = feeDetail.TransactionEntry.FirstOrDefault(x => x.AccountId == feeAccount.AccountId);
                        if (feeEntry != null)
                        {
                            feeEntry.Amount += feeValue;
                            feeEntry.NewBalance += feeValue;
                        }
                        else
                        {
                            decimal feeAccountBalance = await this.GetBalance(feeAccount.AccountId);
                            feeDetail.AddEntry(new TransactionEntry(feeAccount.AccountId, feeValue, 'F', feeAccountBalance, feeAccountBalance + feeValue));
                        }
                    }
                }
                if (fee.TragetAccounId != null)
                {
                    TransactionEntry transactionEntry = userDetail.TransactionEntry.FirstOrDefault(x => x.AccountId == fee.TragetAccounId);
                    if (transactionEntry == null)
                    {
                        decimal accountBalance = await this.GetBalance(fee.TragetAccounId);
                        transactionEntry = new TransactionEntry(fee.TragetAccounId, -userDetail.FiatFee, 'F', accountBalance, accountBalance - userDetail.FiatFee);
                        userDetail.AddEntry(transactionEntry);
                    }
                    else
                    {
                        transactionEntry.Amount -= fee.FeeCalculate.Calculate(userDetail.FiatAmountSubtotal ?? 0);
                        transactionEntry.NewBalance -= userDetail.FiatFee;
                    }
                }
                else
                {
                    throw new Exception($"Didn't fill fee target account id.");
                }
            }
        }

        private async Task FilledTradeFees(IEnumerable<FeeInfo> fees, TransactionDetail userDetail, Transaction transaction, decimal realPrice, decimal amount, int multiplier)
        {
            if (fees == null)
            {
                return;
            }
            foreach (FeeInfo fee in fees)
            {
                Account userFeeAccount = await this.GetAccount(fee.TragetAccounId);
                if (userDetail.UserId != userFeeAccount.UserId)
                {
                    continue;
                }
                decimal feeValueTotal = userFeeAccount.ExtraRound(fee.FeeCalculate.Calculate(realPrice * amount));
                foreach (CollectorInfo collector in fee.Collectors)
                {
                    Account feeAccount = await this.GetAccount(collector.AccountId);
                    CustomValidator.CheckNotNull(feeAccount, $"Fee account '{collector.AccountId}' not exist.");
                    decimal feeValue = feeValueTotal * collector.Value / 100m;
                    TransactionDetail feeDetail = transaction.TransactionDetail.FirstOrDefault(x => x.TransactionEntry.Any(y => y.AccountId == collector.AccountId));
                    if (feeDetail == null)
                    {
                        feeDetail = new TransactionDetail(collector.Comment ?? "Fee", feeAccount.UserId, userDetail.OrderId, userDetail.TradeId, realPrice);
                        feeDetail.TransactionId = transaction.TransactionId;
                        feeDetail.TransactionDetailId = transaction.GetNextDetailId();
                        feeDetail.FiatAmountSubtotal = -multiplier * realPrice * amount;
                        feeDetail.FiatAmountTotal = Math.Abs((feeDetail.FiatAmountSubtotal ?? 0) - feeValue);
                        feeDetail.FiatAmountSubtotal = Math.Abs(feeDetail.FiatAmountSubtotal ?? 0);
                        feeDetail.FiatFee = feeValue;
                        decimal feeAccountBalance = await this.GetBalance(feeAccount.AccountId);
                        feeDetail.AddEntry(new TransactionEntry(feeAccount.AccountId, feeValue, 'F', feeAccountBalance, feeAccountBalance + feeValue));
                        transaction.TransactionDetail.Add(feeDetail);
                    }
                    else
                    {
                        feeDetail.FiatAmountSubtotal = -multiplier *realPrice * amount;
                        feeDetail.FiatAmountTotal = Math.Abs((feeDetail.FiatAmountSubtotal ?? 0) - feeValue);
                        feeDetail.FiatAmountSubtotal = Math.Abs(feeDetail.FiatAmountSubtotal ?? 0);

                        TransactionEntry feeEntry = feeDetail.TransactionEntry.FirstOrDefault(x => x.AccountId == feeAccount.AccountId);
                        if (feeEntry != null)
                        {
                            feeEntry.Amount += feeValue;
                            feeEntry.NewBalance += feeValue;
                        }
                        else
                        {
                            decimal feeAccountBalance = await this.GetBalance(feeAccount.AccountId);
                            feeDetail.AddEntry(new TransactionEntry(feeAccount.AccountId, feeValue, 'F', feeAccountBalance, feeAccountBalance + feeValue));
                        }
                    }
                }
            }
        }

        private void FilledTotal(IEnumerable<FeeInfo> fees, decimal fiatAmount, TransactionDetail userDetail)
        {
            userDetail.FiatAmountSubtotal = Math.Abs(fiatAmount);
            userDetail.FiatAmountTotal = fiatAmount;
            if (fees != null)
            {
                foreach (var fee in fees)
                {
                    Account userFeeAccount = this.GetAccount(fee.TragetAccounId).Result;
                    userDetail.FiatAmountSubtotal = userFeeAccount.ExtraRound(userDetail.FiatAmountSubtotal);
                    userDetail.FiatAmountTotal = userFeeAccount.ExtraRound(userDetail.FiatAmountTotal);
                    if (userDetail.UserId != userFeeAccount.UserId)
                    {
                        continue;
                    }
                    var feeAmount = userFeeAccount.ExtraRound(fee.FeeCalculate.Calculate((decimal)userDetail.FiatAmountSubtotal));
                    userDetail.FiatFee += feeAmount;
                    userDetail.FiatAmountTotal -= feeAmount;
                }
            }
            userDetail.FiatAmountTotal = Math.Abs((decimal)userDetail.FiatAmountTotal);
        }


        public async Task<Transaction> CreateSendTransaction(
            string accountFromId, 
            string accountToId, 
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string userComment = null,
            string sendDescription = null,
            string receiveDescription = null)
        {
            var accountFrom = await this.GetAccount(accountFromId);
            var accountTo = await this.GetAccount(accountToId);

            CustomValidator.Check(amount > 0, "Amount must be positive. {amount}");
            CustomValidator.CheckNotNull(accountFrom, $"Account '{accountFromId}' does not exist.");
            CustomValidator.CheckNotNull(accountToId, $"Account '{accountToId}' does not exist.");
            CustomValidator.Check(accountFrom.CurrencyId == accountTo.CurrencyId, 
                $"Account 'from' {accountFromId}'s currency {accountFrom.CurrencyId} aren't equal account 'to' {accountToId}'s currency: {accountTo.CurrencyId}");

            string fromComment = sendDescription ?? $"Sent to {accountTo.Name}.";
            var detailFrom = new TransactionDetail(fromComment, accountFrom.UserId, userComment: userComment);
            detailFrom.AddEntry(new TransactionEntry(accountFrom.AccountId, -amount, TransactionTypeChar.Send, opposingAccountId: accountTo.AccountId));

            string toComment = receiveDescription ?? $"Received from {accountFrom.Name}.";
            var detailTo = new TransactionDetail(toComment, accountTo.UserId, userComment: userComment);
            detailTo.AddEntry(new TransactionEntry(accountTo.AccountId, amount, TransactionTypeChar.Receive, opposingAccountId: accountFrom.AccountId));

            TransactionDetail[] details = new TransactionDetail[] { detailFrom, detailTo };

            return await CreateTransaction($"Send {amount.ToString(accountFrom.Currency.Formatter)} {accountFrom.CurrencyId}", TransactionTypeChar.Send, transactionStatus, details, fees);
        }

        public async Task<Transaction> CreateFeeTransaction(string accountFromId,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string fromDescription = null)
        {
            Account accountFrom = await this.GetAccount(accountFromId);

            string fromComment = fromDescription ?? $"Take fee.";
            TransactionDetail detailFrom = new TransactionDetail(fromComment, accountFrom.UserId);
            detailFrom.AddEntry(new TransactionEntry(accountFrom.AccountId, 0, 'F'));

            return await CreateTransaction(fromComment, 'F', transactionStatus, detailFrom, fees);
        }

        public async Task<Transaction> CreateTransferTransaction(string accountFromId,
            string accountToId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string userComment = null)
        {
            Account accountFrom = await this.GetAccount(accountFromId);
            Account accountTo = await this.GetAccount(accountToId);

            if (accountFrom.UserId != accountTo.UserId)
            {
                throw new Exception("Transfer can only be between accounts of one user.");
            }

            string fromComment = $"Transfer from {accountFrom.Name} to {accountTo.Name}.";
            TransactionDetail detail = new TransactionDetail(fromComment, accountFrom.UserId, userComment: userComment);
            detail.AddEntry(new TransactionEntry(accountFrom.AccountId, -amount, 'T', opposingAccountId: accountTo.AccountId));
            detail.AddEntry(new TransactionEntry(accountTo.AccountId, amount, 'T', opposingAccountId: accountFrom.AccountId));

            return await CreateTransaction($"Transfer {amount.ToString(accountFrom.Currency.Formatter)} {accountFrom.CurrencyId}", 'T', transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateDepositTransaction(string accountId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string accountIdFrom = null,
            string accountNameFrom = null,
            string userComment = null)
        {
            var accountTo = await this.GetAccount(accountId);
            var accountFrom = await this.GetAccount(accountIdFrom);
            CustomValidator.CheckNotNull(accountTo, $"Account '{accountId}' not exist.");

            bool isUserAccount = accountFrom != null && accountFrom.IsUserAccount();
            string description = $"{(isUserAccount ? "Transfer" : "Received")} from {(accountFrom != null ? accountFrom.Name : accountNameFrom)}";
            var detail = new TransactionDetail(description, accountTo.UserId, userComment: userComment);
            detail.AddEntry(new TransactionEntry(accountTo.AccountId, amount, isUserAccount ? 'T' : 'R', opposingAccountId: accountIdFrom));
            return await CreateTransaction(description, isUserAccount ? 'T' : 'R', transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateSimpleDepositTransaction(string accountId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string description = null,
            string userComment = null)
        {
            var accountTo = await this.GetAccount(accountId);
            CustomValidator.CheckNotNull(accountTo, $"Account '{accountId}' not exist.");

            description = description ?? $"Deposit {Assets.GetIcon(accountTo.CurrencyId)} {amount.ToString("N2")}";
            var detail = new TransactionDetail(description, accountTo.UserId, userComment: userComment);
            detail.AddEntry(new TransactionEntry(accountTo.AccountId, amount, 'D'));
            return await CreateTransaction(description, 'D', transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateWithdrawTransaction(string accountId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string accountIdTo = null,
            string userComment = null)
        {
            Account accountFrom = await this.GetAccount(accountId);
            Account accountTo = await this.GetAccount(accountIdTo);
            bool isUserAccount = accountTo != null && accountTo.IsUserAccount();
            string comment = $"{(isUserAccount ? "Transfer" : "Send")} to {accountTo.Name}";
            TransactionDetail detail = new TransactionDetail(comment, accountFrom.UserId, userComment: userComment);
            detail.AddEntry(new TransactionEntry(accountFrom.AccountId, -amount, isUserAccount ? TransactionTypeChar.Transfer : TransactionTypeChar.Send, opposingAccountId: accountIdTo));

            return await CreateTransaction(comment, isUserAccount ? TransactionTypeChar.Transfer : TransactionTypeChar.Send, transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateSimpleWithdrawTransaction(string accountId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed,
            string userComment = null)
        {
            Account accountFrom = await this.GetAccount(accountId);
            string comment = $"Withdraw {Assets.GetIcon(accountFrom.CurrencyId)} {amount.ToString("N2")}";
            TransactionDetail detail = new TransactionDetail(comment, accountFrom.UserId, userComment: userComment);
            detail.AddEntry(new TransactionEntry(accountFrom.AccountId, -amount, 'W'));

            return await CreateTransaction(comment, 'W', transactionStatus, detail, fees);
        }




        public async Task<Transaction> CreateCancelTransaction(string transactionId)
        {
            string comment = $"Refund. Cancel transaction '{transactionId}'.";
            using (var context = new AccountContext())
            {
                var transaction = await context.Transaction
                    .Include(x => x.TransactionDetail).Include($"{nameof(TransactionDetail)}.{nameof(TransactionEntry)}")
                    .FirstOrDefaultAsync(x => x.TransactionId == transactionId);
                Transaction cancelTransaction = new Transaction()
                {
                    TransactionId = NewID(),
                    Description = comment,
                    TotalFee = 0,
                    TransactionStateId = 'C',
                    TransactionTypeId = 'X',
                    Ts = DateTime.UtcNow,
                    TransactionDetail = new List<TransactionDetail>()
                };
                foreach (TransactionDetail transactionDetail in transaction.TransactionDetail)
                {
                    TransactionDetail cancelTransactionDetail = new TransactionDetail()
                    {
                        Comment = comment,
                        FiatAmountSubtotal = 0,
                        FiatAmountTotal = 0,
                        FiatFee = 0,
                        TransactionDetailId = NewID(),
                        TransactionId = cancelTransaction.TransactionId,
                        UserId = transactionDetail.UserId,
                        TransactionEntry = new List<TransactionEntry>()
                    };
                    cancelTransaction.TransactionDetail.Add(cancelTransactionDetail);
                    foreach (TransactionEntry transactionEntry in transactionDetail.TransactionEntry)
                    {
                        TransactionEntry cancelTransactionEntry = new TransactionEntry()
                        {
                            Amount = -transactionEntry.Amount,
                            AccountId = transactionEntry.AccountId,
                            NewBalance = transactionEntry.PrevBalance,
                            PrevBalance = transactionEntry.NewBalance,
                            TransactionType = 'X',
                            OpposingAccountId = transactionEntry.OpposingAccountId,
                            TransactionDetailId = cancelTransactionDetail.TransactionDetailId
                        };
                        cancelTransactionDetail.TransactionEntry.Add(cancelTransactionEntry);
                    }
                }
                return cancelTransaction;
            }
        }

        public async Task<Transaction> CreateDepositCryptoTransaction(string accountId,
            decimal amount,
            decimal price,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed)
        {
            Account account = await this.GetAccount(accountId);
            TransactionDetail detail = new TransactionDetail(
                comment: $"Deposit {amount.ToString(account.Currency.Formatter)} {Assets.GetIcon(account.CurrencyId)}",
                userId: account.UserId,
                price: price);
            detail.AddEntry(new TransactionEntry()
            {
                AccountId = accountId,
                Amount = amount,
                TransactionType = 'D'
            });
            return await CreateTransaction(detail.Comment, 'D', transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateWithdrawCryptoTransaction(string accountId,
            decimal amount,
            decimal price,
            IEnumerable<FeeInfo> fees,
            TransactionStatus transactionStatus = TransactionStatus.Completed)
        {
            Account account = await this.GetAccount(accountId);
            TransactionDetail detail = new TransactionDetail(
                comment: $"Withdraw {amount.ToString(account.Currency.Formatter)} {Assets.GetIcon(account.CurrencyId)}",
                userId: account.UserId,
                price: price);
            detail.AddEntry(new TransactionEntry()
            {
                AccountId = accountId,
                Amount = -amount,
                TransactionType = 'W'
            });
            return await CreateTransaction(detail.Comment, 'W', transactionStatus, detail, fees);
        }

        public async Task<Transaction> CreateTradeTransaction(IEnumerable<Execution> executions, TransactionStatus transactionStatus = TransactionStatus.Completed, string assetId = Assets.UsdAssetId,
            IEnumerable<FeeInfo> fees = null)
        {
            List<TransactionDetail> details = new List<TransactionDetail>();
            foreach (var item in executions.GroupBy(x => x.UserId))
            {
                TransactionDetail detail = new TransactionDetail()
                {
                    Comment =
                        $"{(item.First().GetActionName(transactionStatus))} {item.Sum(x => x.Amount).ToString("N")} {item.FirstOrDefault().Symbol} for {item.Average(x => x.Price):F2} {assetId} each",
                    UserId = item.Key,
                    OrderId = item.First().OrderId,
                    TradeId = item.First().TradeId,
                    Price = item.Average(x => x.Price),
                };
                foreach (Execution execution in item)
                {
                    Account dirrectAccount = this.GetAccountByCurrencyId(detail.UserId, execution.Symbol);
                    Account oppositeAccount = this.GetAccountByCurrencyId(detail.UserId, assetId);
                    var directEntry = detail.TransactionEntry.FirstOrDefault(x => x.AccountId == dirrectAccount.AccountId);
                    if (directEntry != null)
                    {
                        directEntry.Amount += execution.Multiplier * execution.Amount;
                        var oppositeEntry = detail.TransactionEntry.FirstOrDefault(x => x.AccountId == directEntry.OpposingAccountId);
                        oppositeEntry.Amount += - execution.Multiplier * execution.Amount * execution.Price;
                    }
                    else
                    {
                        detail.AddEntry(new TransactionEntry()
                        {
                            AccountId = oppositeAccount.AccountId,
                            Amount = - execution.Multiplier * execution.Amount * execution.Price,
                            TransactionType = execution.IsBuy ? TransactionTypeChar.Sell : TransactionTypeChar.Buy,
                            OpposingAccountId = dirrectAccount.AccountId
                        });
                        detail.AddEntry(new TransactionEntry()
                        {
                            AccountId = dirrectAccount.AccountId,
                            Amount = execution.Multiplier * execution.Amount,
                            TransactionType = execution.IsBuy ? TransactionTypeChar.Buy : TransactionTypeChar.Sell,
                            OpposingAccountId = oppositeAccount.AccountId
                        });
                    }
                }

                details.Add(detail);
            }

            return await CreateTransaction($"Exch {executions.First().Symbol} to USD", 'E', transactionStatus, details, fees);
        }


        public async Task<Transaction> UpdateTradeTransaction(string transactionId, Execution execution, TransactionStatus transactionStatus, FeeInfo displayFee, IEnumerable<FeeInfo> fees, string assetId = Assets.UsdAssetId)
        {
            Transaction transaction = await this.GetTransaction(transactionId);
            CustomValidator.CheckNotNull(transaction, $"Transaction with id = '{transactionId}' not exist.");
            transaction.TransactionStateId = (char)transactionStatus;
            TransactionDetail transactionDetail = transaction.TransactionDetail.FirstOrDefault(x => x.UserId == execution.UserId);
            CustomValidator.CheckNotNull(transactionDetail, $"Transaction with id = '{transactionId}' does not contain user with id '{execution.UserId}'.");
            transactionDetail.OrderId = execution.OrderId;
            transactionDetail.TradeId = execution.TradeId;
            decimal displayPrice = GetDisplayPrice(execution, displayFee, fees);
            transactionDetail.Price = displayPrice;
            decimal usdAmount = displayPrice * execution.Amount;


            this.FilledTotal(new FeeInfo[] { displayFee }, (execution.IsBuy? -1 : 1) * usdAmount, transactionDetail);
            //var mult = UpdateCommissions(transaction, usdAmount);

            

            TransactionEntry entrySymbol = GetBuySellEntry(execution.Symbol, transactionDetail);
            Account account = await this.GetAccount(entrySymbol.AccountId);
            CustomValidator.CheckNotNull(entrySymbol, $"Transaction with id = '{transactionId}' does not contain entry for '{execution.Symbol}'.");
            entrySymbol.NewBalance = entrySymbol.NewBalance - entrySymbol.Amount + execution.Multiplier * execution.Amount;
            entrySymbol.Amount = account.ExtraRound(execution.Multiplier * execution.Amount);

            TransactionEntry entryUsd = GetBuySellEntry(assetId, transactionDetail);

            CustomValidator.CheckNotNull(transaction, $"Transaction with id = '{transactionId}' does not contain entry '{assetId}'.");
            entryUsd.NewBalance = entryUsd.NewBalance - execution.Multiplier * transactionDetail.FiatAmountTotal ?? 0;
            entryUsd.Amount = - execution.Multiplier * transactionDetail.FiatAmountTotal ?? 0;
            decimal caclPrice = this.GetCalcPrice(execution, displayPrice);
            await this.FilledTradeFees(fees, transactionDetail, transaction, caclPrice, execution.Amount, execution.Multiplier);

            if (transactionStatus == TransactionStatus.Completed)
            {
                transactionDetail.Comment = $"{(execution.IsBuy ? "Bought" : "Sold")} { account.Round(execution.Amount).ToString("N")} {execution.Symbol} for {displayPrice:F2} {assetId} each";
            }
            else if (transactionStatus == TransactionStatus.Rejected)
            {
                transactionDetail.Comment = "Order canceled";
            }
            

            return transaction;
        }

        private decimal GetFixFiatAmount(Execution execution, IEnumerable<FeeInfo> fees)
        {
            decimal total = execution.Amount * execution.Price;
            decimal realFee = fees.Sum(x => x.FeeCalculate.Calculate(1));
            decimal realTotal = total - execution.Multiplier * realFee * total;
            return realTotal / execution.Price;
        }

        private decimal GetCalcPrice(Execution execution, decimal displayPrice)
        {
            switch (execution.TradeType)
            {
                case TradeType.FixFiat:
                    return displayPrice;
                case TradeType.FixCoin:
                case TradeType.Limit:
                    return execution.Price;
                default:
                    throw new Exception($"Unknown trade type: {execution.TradeType}");
            }
        }

        private static decimal GetDisplayPrice(Execution execution, FeeInfo displayFee, IEnumerable<FeeInfo> fees)
        {
            switch (execution.TradeType)
            {
                case TradeType.FixFiat:

                    {
                        decimal realFee = fees.Sum(x => x.FeeCalculate.Calculate(1));
                        decimal totalCost = execution.Price * execution.Amount / (1 - execution.Multiplier * realFee);
                        return totalCost / execution.Amount;
                    }
                case TradeType.FixCoin:
                    {
                        decimal displayFeeValue = displayFee.FeeCalculate.Calculate(1);
                        decimal fullPrice = execution.Price + execution.Multiplier * (fees.Sum(x => x.FeeCalculate.Calculate(execution.Price)));
                        return fullPrice / (1 + execution.Multiplier * displayFeeValue);
                    }
                case TradeType.Limit:
                    {
                        return execution.Price;
                    }
                default:
                    {
                        throw new Exception($"Unsupported trade type {execution.TradeType}");
                    }
            }
        }

        private TransactionEntry GetBuySellEntry(string assetId, TransactionDetail transactionDetail)
        {
            var accountId = GetAccountByCurrencyId(transactionDetail.UserId, assetId).AccountId;

            return transactionDetail.TransactionEntry.FirstOrDefault(x => x.AccountId == accountId &&
                                                                          (x.TransactionType == TransactionTypeChar.Buy || x.TransactionType == TransactionTypeChar.Sell));
        }

        public async Task<Transaction> CreateTestTransaction(string accountId,
            decimal amount,
            IEnumerable<FeeInfo> fees,
            string description,
            char transactionType,
            char entryType,
            TransactionStatus transactionStatus = TransactionStatus.Completed)
        {
            var account = await this.GetAccount(accountId);

            var detail = new TransactionDetail(description, account.UserId);
            detail.AddEntry(new TransactionEntry(account.AccountId, amount, entryType));
            return await CreateTransaction(description, transactionType, transactionStatus, detail, fees);
        }

        //public async Task<Transaction> CreateSendTokenTransaction(string userId, decimal amount, string symbol, decimal pricePerCoin, TransactionInfo transactionInfo, string assetId = Assets.UsdAssetId)
        //{
        //    List<TransactionDetailModel> details = new List<TransactionDetailModel>()
        //    {
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Send {amount.ToCommaSeparatedString()} {symbol}",
        //            FeeId = FeeConstants.TransferOutgoingFeeId,
        //            UserId = userId,
        //            Price = pricePerCoin,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = symbol,
        //                    Amount = -amount,
        //                    Type = 'N'
        //                }
        //            }
        //        }

        //    };
        //    return await CreateTransaction($"Send {symbol}", 'N', TransactionStatus.Pending, details, transactionInfo: transactionInfo);
        //}

        //public async Task<Transaction> CreateSendTokenToBotTransaction(string userIdFrom, string userIdTo, decimal amount, string symbol, decimal pricePerCoin, string assetId = Assets.UsdAssetId)
        //{
        //    List<TransactionDetailModel> details = new List<TransactionDetailModel>()
        //    {
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Transfer {amount.ToCommaSeparatedString()} {symbol} to bot.",
        //            FeeId = FeeConstants.NoFee,
        //            UserId = userIdFrom,
        //            Price = pricePerCoin,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = symbol,
        //                    Amount = -amount,
        //                    Type = 'I'
        //                }
        //            }
        //        },
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Transfer {amount.ToCommaSeparatedString()} {symbol} from issuer.",
        //            FeeId = FeeConstants.NoFee,
        //            UserId = userIdTo,
        //            Price = pricePerCoin,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = symbol,
        //                    Amount = amount,
        //                    Type = 'I'
        //                }
        //            }
        //        }

        //    };
        //    return await CreateTransaction($"Transfer {symbol} to bot", 'I', TransactionStatus.Completed, details);
        //}

        //public async Task<Transaction> CreateNewTransferTransaction(string fromAccountId, string toAccountId, decimal amount)
        //{
        //    Account fromAccount = await this.GetAccount(fromAccountId);
        //    Account toAccount = await this.GetAccount(toAccountId);
        //    List<TransactionDetailModel> details = new List<TransactionDetailModel>()
        //    {
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Sent {amount.ToCommaSeparatedString()} {fromAccount.CurrencyId}.",
        //            FeeId = FeeConstants.TransferInconingFeeId,
        //            UserId = fromAccount.UserId,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = fromAccountId,
        //                    OppositeAssetId = toAccountId,
        //                    Amount = -amount,
        //                    Type = 'T'
        //                }
        //            }
        //        },
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Received {amount.ToCommaSeparatedString()} {fromAccount.CurrencyId}.",
        //            FeeId = FeeConstants.TransferInconingFeeId,
        //            UserId = toAccount.UserId,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = toAccountId,
        //                    OppositeAssetId = fromAccountId,
        //                    Amount = amount,
        //                    Type = 'T'
        //                }
        //            }
        //        }

        //    };
        //    return await CreateTransaction($"Transfer {amount} {fromAccount.CurrencyId}", 'T', TransactionStatus.Completed, details);
        //}

        //public async Task<Transaction> CreateReceiveTokenTransaction(string userId, decimal amount, string symbol, decimal pricePerCoin, TransactionInfo transactionInfo, string assetId = Assets.UsdAssetId)
        //{
        //    List<TransactionDetailModel> details = new List<TransactionDetailModel>()
        //    {
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Receive {amount.ToCommaSeparatedString()} {symbol}",
        //            FeeId = FeeConstants.TransferInconingFeeId,
        //            UserId = userId,
        //            Price = pricePerCoin,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = symbol,
        //                    Amount = amount,
        //                    Type = 'R'
        //                }
        //            }
        //        }

        //    };
        //    return await CreateTransaction($"Receive {symbol}", 'R', TransactionStatus.Completed, details, transactionInfo: transactionInfo);
        //}

        //public async Task<Transaction> CreateMintedTokenTransaction(string userId, decimal amount, string symbol, string assetId = Assets.UsdAssetId)
        //{
        //    TransactionDetailModel[] details = new TransactionDetailModel[]
        //    {
        //        new TransactionDetailModel()
        //        {
        //            Comment = $"Issuer {symbol} got {amount.ToCommaSeparatedString()} tokens",
        //            UserId = userId,
        //            Price = 0,
        //            FeeId = FeeConstants.MintedTokenFee,
        //            Entries = new List<TransactionEntryModel>()
        //            {
        //                new TransactionEntryModel()
        //                {
        //                    AssetId = symbol,
        //                    Amount = amount,
        //                    Type = 'M'
        //                },
        //            }
        //        }
        //    };
        //    return await CreateTransaction($"Issuer {symbol} got {amount.ToCommaSeparatedString()} tokens", 'M', TransactionStatus.Completed, details);
        //}
    }
}
