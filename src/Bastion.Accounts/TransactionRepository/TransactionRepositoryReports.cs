﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Constants;
    using Bastion.Accounts.Context;
    using Bastion.Accounts.ExtraModels;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Transactions;
    using Bastion.Helper;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public partial class TransactionRepository
    {
        public async Task<IEnumerable<TransactionReport>> GetTransactionReport(string accountId, string fiatCurrencyId = Assets.UsdAssetId, DateTime? from = null, DateTime? to = null)
        {
            var transactions = await GetTransactions(accountId, from, to);
            List<TransactionReport> transactionReports = new List<TransactionReport>();
            foreach (var transactionOperation in transactions)
            {
                TransactionReport transactionReport = await GetTransactionReportAsync(accountId, transactionOperation, fiatCurrencyId);
                transactionReports.Add(transactionReport);
            }

            return transactionReports.OrderByDescending(x => x.Date);
        }

        public async Task<IEnumerable<ShortTransactionReport>> GetNewTransactionReport(string accountId, string fiatCurrencyId = Assets.UsdAssetId, string fromUserId = null, string toUserId = null, string fromAccountId = null, string toAccountId = null)
        {
            var transactions = await GetTransactions(accountId);
            Currency fiatCurrency = await this.GetCurrency(fiatCurrencyId);
            Account account = await this.GetAccount(accountId);
            List<ShortTransactionReport> reports = new List<ShortTransactionReport>();
            foreach (var transactionDetail in transactions)
            {
                TransactionEntry entry = transactionDetail.TransactionEntry.First(x => x.AccountId == accountId);
                if (!this.IsSearchSuccess(entry, fromUserId, toUserId, fromAccountId, toAccountId))
                {
                    continue;
                }
                reports.Add(new ShortTransactionReport(transactionDetail, account, fiatCurrency));
            }
            return reports;
        }

        private bool IsSearchSuccess(TransactionEntry entry, string fromUserId, string toUserId, string fromAccountId, string toAccountId)
        {
            if (fromUserId == null && toUserId == null && fromAccountId == null && toAccountId == null)
            {
                return true;
            }
            if (fromUserId != null)
            {
                if (entry.OpposingAccount?.UserId == fromUserId && entry.Amount > 0)
                {
                    return true;
                }
            }
            if (toUserId != null)
            {
                if (entry.OpposingAccount?.UserId == toUserId && entry.Amount < 0)
                {
                    return true;
                }
            }
            if (fromAccountId != null)
            {
                if (entry.OpposingAccountId == fromAccountId && entry.Amount > 0)
                {
                    return true;
                }
            }
            if (toAccountId != null)
            {
                if (entry.OpposingAccountId == toAccountId && entry.Amount < 0)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<TransactionReport> GetTransactionReport(string referenceCode, string accountId, string fiatCurrencyId = Assets.UsdAssetId)
        {
            var transaction = await GetTransaction(referenceCode);
            List<TransactionReport> transactionReports = new List<TransactionReport>();
            Account account = await this.GetAccount(accountId);
            TransactionDetail transactionDetail = transaction.TransactionDetail.FirstOrDefault(x => x.UserId == account.UserId) ?? throw new System.Exception($"Account '{accountId}' did not participate in transaction {referenceCode}");
            return await GetTransactionReportAsync(accountId, transactionDetail, fiatCurrencyId);
        }

        public async Task<TransactionReport> GetTransactionReportAsync(string assetId, TransactionDetail transactionDetail, string fiatCurrencyId = Assets.UsdAssetId)
        {
            Account account = this.GetAccount(assetId).Result;
            Currency fiatCurrency = await this.GetCurrency(fiatCurrencyId);
            TransactionReport transactionReport = new TransactionReport(transactionDetail, account, fiatCurrency);

            ///???
            if (transactionDetail.Transaction.TransactionTypeId == 'X')
            {
                var code = regEx.Match(transactionDetail.Transaction.Description).ToString();
                code = code.Replace("'", string.Empty);
                transactionReport.TargetReferenceCode = code;
            }
            //???
            if (transactionReport.Type == "D" || transactionReport.Type == "B" || transactionReport.Type == "S")
            {
                transactionReport.PaymentMethod = $"cash ({account.CurrencyId})";
            }

            return transactionReport;
        }

        public TransactionReport GetTransactionReport(string assetId, TransactionDetail transactionDetail, string fiatCurrencyId = Assets.UsdAssetId)
        {
            return GetTransactionReportAsync(assetId, transactionDetail, fiatCurrencyId).Result;
        }

        public async Task<TransactionReport> GetLastTransactionReport(string accountId, string fiatCurrencyId = Assets.UsdAssetId, DateTime? to = null)
        {
            var transactionOperation = GetLastTransaction(accountId, to);
            if (transactionOperation == null)
            {
                return null;
            }
            TransactionReport transactionReport = await GetTransactionReportAsync(accountId, transactionOperation, fiatCurrencyId);
            return transactionReport;
        }

        public async Task<Dictionary<string, IList<TransactionReport>>> GetTransactionReportsAsync(Transaction transaction, string fiatCurrencyId = Assets.UsdAssetId)
        {
            var transactionReports = new Dictionary<string, IList<TransactionReport>>();
            foreach (var transactionDetail in transaction.TransactionDetail)
            {
                if (!transactionReports.TryGetValue(transactionDetail.UserId, out IList<TransactionReport> userReports))
                {
                    userReports = new List<TransactionReport>();
                    transactionReports.Add(transactionDetail.UserId, userReports);
                }
                foreach (var transactionEntry in transactionDetail.TransactionEntry)
                {
                    userReports.Add(await GetTransactionReportAsync(transactionEntry.AccountId, transactionDetail, fiatCurrencyId));
                }
            }
            return transactionReports;
        }

        public Dictionary<string, IList<TransactionReport>> GetTransactionReports(Transaction transaction)
        {
            return this.GetTransactionReportsAsync(transaction).Result;
        }

        public IEnumerable<TransactionOperation> GetTransactionOperations(string userId, int count)
        {
            using (var context = new AccountContext())
            {
                var transactionDetails = context.TransactionDetail
                    .Where(x => x.UserId == userId)
                    .OrderByDescending(x => x.Transaction.Ts)
                    .Take(count)
                    .Include(x => x.Transaction)
                    .Include(x => x.TransactionEntry)
                    .ToList();

                foreach (TransactionDetail transactionDetail in transactionDetails)
                {
                    TransactionOperation transactionOperation = new TransactionOperation()
                    {
                        Date = transactionDetail.Transaction.Ts,
                        PricePerCoin = transactionDetail.Price,
                        Status = transactionDetail.Transaction.TransactionStateId,
                        Fee = transactionDetail.FiatFee,
                        OrderId = transactionDetail.OrderId,
                        SubTotal = transactionDetail.FiatAmountSubtotal,
                        Total = transactionDetail.FiatAmountTotal,
                        Description = transactionDetail.Comment,
                        ReferenceCode = transactionDetail.TransactionId

                    };

                    TransactionEntry cryptoEntry = transactionDetail.TransactionEntry.FirstOrDefault(x => !Assets.IsFiatAsset(x.AccountId));
                    if (cryptoEntry != null)
                    {
                        transactionOperation.Type = cryptoEntry.TransactionType.ToString();
                        transactionOperation.Amount = cryptoEntry.Amount;
                        transactionOperation.Symbol = cryptoEntry.AccountId;
                        transactionOperation.NewBalance = cryptoEntry.NewBalance;
                        TransactionEntry fiatEntry = transactionDetail.TransactionEntry.FirstOrDefault(x => Assets.IsFiatAsset(x.AccountId));
                        if (fiatEntry != null)
                        {
                            transactionOperation.AmountUsd = fiatEntry.Amount;
                        }
                    }
                    else
                    {
                        TransactionEntry fiatEntry = transactionDetail.TransactionEntry.FirstOrDefault(x => Assets.IsFiatAsset(x.AccountId));
                        if (fiatEntry != null)
                        {
                            transactionOperation.Amount = fiatEntry.Amount;
                            transactionOperation.Symbol = fiatEntry.AccountId;
                            transactionOperation.Type = fiatEntry.TransactionType.ToString();
                            transactionOperation.NewBalance = fiatEntry.NewBalance;
                        }
                    }
                    if (transactionOperation.Type == "D" || transactionOperation.Type == "B" || transactionOperation.Type == "S")
                    {
                        transactionOperation.PaymentMethod = "cash (USD)";
                    }
                    yield return transactionOperation;
                }
            }
        }
    }
}
