﻿namespace Bastion.Accounts.ExtraModels
{
    public class Amount
    {
        public string Currency { get; set; }
        public decimal Value { get; set; }
        public short? DecimalPoints { get; set; }

        public Amount()
        {

        }

        public Amount(string currency, decimal value, short? decimalPoints)
        {
            this.Currency = currency;
            this.Value = value;
            this.DecimalPoints = decimalPoints;
        }
    }
}
