﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Constants;
    using Bastion.Accounts.ExtraModels;
    using Bastion.Accounts.Helpers;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Transactions;
    using Bastion.Helper;
    using Bastion.Notifications.Abstractions;
    using Newtonsoft.Json;

    using System;
    using System.Linq;

    public class TransactionReport : INotifiable
    {
        public TransactionReport()
        {

        }
        public TransactionReport(TransactionDetail transactionDetail, Account account, Currency fiatCurrency)
        {
            this.TransactionType = transactionDetail.Transaction.TransactionTypeId;
            TransactionEntry transactionOperation = transactionDetail.TransactionEntry.First(x => x.AccountId == account.AccountId);
            AccountId = account.AccountId;
            Amount = account.GetStringAmount(transactionOperation.Amount);
            AmountValue = transactionOperation.Amount;
            Fee = this.GetFiatString(transactionDetail.FiatFee, fiatCurrency.Symbol);
            FeeValue = transactionDetail.FiatFee;
            if (transactionDetail.Transaction.TransactionStateId == (char)TransactionStatus.Completed)
            {
                NewBalance = account.GetStringAmount(transactionOperation.NewBalance);
                NewBalanceValue = transactionOperation.NewBalance;
                OldBalance = account.GetStringAmount(transactionOperation.PrevBalance);
                OldBalanceValue = transactionOperation.PrevBalance;
            }
            else
            {
                NewBalance = "-";
                OldBalance = "-";
            }
            
            OrderId = transactionOperation.TransactionDetail.OrderId;
            PricePerCoin = account.GetStringAmount(transactionDetail.Price);

            int totalMultiprlier = GetTotalMultiplier(transactionOperation, account);
            SubTotal = this.GetFiatString(transactionOperation.TransactionDetail.FiatAmountSubtotal * totalMultiprlier, fiatCurrency.Symbol);
            SubTotalValue = transactionOperation.TransactionDetail.FiatAmountSubtotal ?? 0;
            Total = this.GetFiatString( transactionOperation.TransactionDetail.FiatAmountTotal * totalMultiprlier, fiatCurrency.Symbol);
            TotalValue = transactionOperation.TransactionDetail.FiatAmountTotal ?? 0;
            Date = transactionOperation.TransactionDetail.Transaction.Ts;
            Type = TransactionTypeConverter.GetString(transactionOperation.TransactionType);
            Status = transactionOperation.TransactionDetail.Transaction.TransactionStateId;
            Description = transactionOperation.TransactionDetail.Comment;
            UserComment = transactionOperation.TransactionDetail.UserComment;
            ReferenceCode = transactionOperation.TransactionDetail.TransactionId;
            Source = transactionOperation.TransactionDetail.Transaction.Source;
            Destination = transactionOperation.TransactionDetail.Transaction.Destination;
            TransactionHash = transactionOperation.TransactionDetail.Transaction.NetworkTransactionHash;
            Confirmations = transactionOperation.TransactionDetail.Transaction.Confirmations;
            TradeId = transactionDetail.TradeId;
            this.FeeId = transactionDetail.Transaction.FeeId;

            if (transactionOperation.OpposingAccount != null)
            {
                if (transactionOperation.Amount > 0)
                {
                    this.FromAccount = new AccountView(transactionOperation.OpposingAccount);
                }
                else
                {
                    this.ToAccount = new AccountView(transactionOperation.OpposingAccount);
                }
                TransactionEntry opposingEntry = transactionDetail.TransactionEntry.FirstOrDefault(x => x.AccountId == transactionOperation.OpposingAccountId);
                if (opposingEntry != null && Assets.IsFiatAsset(account.CurrencyId) && opposingEntry.Account.Currency != null)
                {
                    OpposingAmount = opposingEntry.Account.GetStringAmount(opposingEntry.Amount);
                }

            }
        }

        private int GetTotalMultiplier(TransactionEntry transactionOperation, Account account)
        {
            if (Assets.IsFiatAsset(account.CurrencyId))
            {
                return Math.Sign(transactionOperation.Amount);
            }
            else
            {
                if (transactionOperation.TransactionType == TransactionTypeChar.Buy
                    || transactionOperation.TransactionType == TransactionTypeChar.Withdraw
                    || transactionOperation.TransactionType == TransactionTypeChar.Send)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }

        private Amount GetFiatAmmount(decimal? amount, string fiatCurrency)
        {
            if (amount == null)
            {
                return null;
            }

            return new Amount(fiatCurrency, amount.Value, 2);
        }

        private string GetFiatString(decimal? amount, char fiatSymbol)
        {
            if (amount == null)
            {
                return null;
            }

            return fiatSymbol + amount.Value.ToString("n2");
        }

        public string AccountId { get; set; }
        public string Type { get; set; }

        [JsonIgnore]
        public char TransactionType { get; set; }
        public DateTime Date { get; set; }
        public string Fee { get; set; }

        public decimal FeeValue { get; set; }
        public string Amount { get; set; }

        public decimal AmountValue { get; set; }
        public string OpposingAmount { get; set; }

        public decimal SubTotalValue { get; set; }
        public string SubTotal { get; set; }
        public string Total { get; set; }
        public decimal TotalValue { get; set; }
        public string NewBalance { get; set; }
        public decimal NewBalanceValue { get; }
        public string OldBalance { get; set; }

        public decimal OldBalanceValue { get; set; }
        public string PricePerCoin { get; set; }
        public string OrderId { get; set; }
        public string TradeId { get; set; }
        public string ReferenceCode { get; set; }
        public string PaymentMethod { get; set; }
        public string TargetReferenceCode { get; set; }
        public string FeeId { get; set; }
        public AccountView FromAccount { get; set; }
        public AccountView ToAccount { get; set; }

        [JsonIgnore]
        public bool IsPending
        {
            get
            {
                return Status == 'P';
            }
        }

        [JsonIgnore]
        public char Status { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string ShowStatus
        {
            get
            {
                switch (this.Status)
                {
                    case 'C': return "Completed";
                    case 'P': return "Pending";
                    case 'R': return "Rejected";
                    default: return Status.ToString();
                }
            }
        }

        public string Description { get; set; }
        public string UserComment { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string TransactionHash { get; set; }
        public int? Confirmations { get; set; }

        public string NotificationMethod => "onTransaction";
    }
}
