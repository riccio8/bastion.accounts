﻿using Bastion.Accounts.Models;
using System.Linq;

namespace Bastion.Accounts.Transactions
{
    public class AccountView
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }

        public AccountView()
        {

        }
        public AccountView(Account account)
        {
            this.Id = account.AccountId;
            this.Name = account.Name;
            this.Number = $"...{new string(account.AccountNumber.Skip(account.AccountNumber.Length - 4).ToArray())}";
        }
    }
}
