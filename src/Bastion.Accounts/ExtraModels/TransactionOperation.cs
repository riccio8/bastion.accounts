﻿namespace Bastion.Accounts.Transactions
{
    using Newtonsoft.Json;
    using System;

    public class TransactionOperation
    {
        public string Type { get; set; }
        public decimal Fee { get; set; }
        public decimal Amount { get; set; }
        public decimal? AmountUsd { get; set; }
        public string Symbol { get; set; }
        public DateTime Date { get; set; }
        public decimal? PricePerCoin { get; set; }
        public decimal NewBalance { get; set; }
        public string OrderId { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Total { get; set; }
        public string Description { get; set; }
        public string ReferenceCode { get; set; }
        public string PaymentMethod { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string ShowStatus
        {
            get
            {
                switch (this.Status)
                {
                    case 'C': return "Completed";
                    case 'P': return "Pending";
                    case 'R': return "Rejected";
                    default: return Status.ToString();
                }
            }
        }
        [JsonIgnore]
        public char Status { get; set; }
    }
}
