﻿namespace Bastion.Accounts.Transactions
{
    public class TransactionInfo
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public string TransactionHash { get; set; }
        public int Confirmations { get; set; }
    }
}
