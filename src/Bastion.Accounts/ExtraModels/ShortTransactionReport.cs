﻿using Bastion.Accounts.Constants;
using Bastion.Accounts.Helpers;
using Bastion.Accounts.Models;
using Bastion.Accounts.Transactions;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace Bastion.Accounts.ExtraModels
{
    public class ShortTransactionReport
    {
        public string ReferenceCode { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }

        public string Amount { get; set; }
        [JsonIgnore]
        public decimal AmountValue { get; set; }

        public string Total { get; set; }
        [JsonIgnore]
        public decimal TotalValue { get; set; }

        public string NewBalance { get; set; }
        [JsonIgnore]
        public decimal NewBalanceValue { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public char Status { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string ShowStatus
        {
            get
            {
                switch ((TransactionStatus)this.Status)
                {
                    case TransactionStatus.Completed: return TransactionStatus.Completed.ToString();
                    case TransactionStatus.Pending: return TransactionStatus.Pending.ToString();
                    case TransactionStatus.Rejected: return TransactionStatus.Rejected.ToString();
                    default: return Status.ToString();
                }
            }
        }

        public ShortTransactionReport()
        {

        }

        public ShortTransactionReport(TransactionDetail transactionDetail, Account account, Currency fiatCurrency)
        {
            this.ReferenceCode = transactionDetail.TransactionId;
            this.Date = transactionDetail.Transaction.Ts;
            this.Status = transactionDetail.Transaction.TransactionStateId;

            TransactionEntry transactionEntry = transactionDetail.TransactionEntry.First(x => x.AccountId == account.AccountId);
            this.Type = TransactionTypeConverter.GetString(transactionEntry.TransactionType);
            this.Amount = account.GetStringAmount(transactionEntry.Amount);
            this.AmountValue = transactionEntry.Amount;
            this.Description = transactionDetail.Comment;

            int totalMultiprlier = GetTotalMultiplier(transactionEntry, account);
            this.Total = this.GetFiatString(transactionEntry.TransactionDetail.FiatAmountTotal * totalMultiprlier, fiatCurrency.Symbol);
            this.TotalValue = (transactionEntry.TransactionDetail.FiatAmountTotal ?? 0) * totalMultiprlier;

            if (transactionDetail.Transaction.TransactionStateId == (char)TransactionStatus.Completed)
            {
                this.NewBalance = account.GetStringAmount(transactionEntry.NewBalance);
                this.NewBalanceValue = transactionEntry.NewBalance;
            }
            else
            {
                this.NewBalance = "-";
            }
        }

        private string GetFiatString(decimal? amount, char fiatSymbol)
        {
            if (amount == null)
            {
                return null;
            }

            return fiatSymbol + amount.Value.ToString("n2");
        }

        private int GetTotalMultiplier(TransactionEntry transactionOperation, Account account)
        {
            if (Assets.IsFiatAsset(account.CurrencyId))
            {
                return Math.Sign(transactionOperation.Amount);
            }
            else
            {
                if (transactionOperation.TransactionType == TransactionTypeChar.Buy
                    || transactionOperation.TransactionType == TransactionTypeChar.Withdraw
                    || transactionOperation.TransactionType == TransactionTypeChar.Send)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }

    }
}
