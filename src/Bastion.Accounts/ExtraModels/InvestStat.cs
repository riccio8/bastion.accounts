﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Transactions
{
    public class InvestStat
    {
        public decimal Amount { get; set; }
        public decimal? USD { get; set; }
    }
}
