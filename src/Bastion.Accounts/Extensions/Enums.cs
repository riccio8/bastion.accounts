﻿namespace Bastion.Accounts.Extensions
{
    public enum OfferingStage: byte
    {
        None = (byte)'N',
        Primary = (byte)'P',
        Secondary = (byte) 'S'
    }
}
