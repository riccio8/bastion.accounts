﻿using Bastion.Accounts.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Extensions
{
    public static class ServiceCollectionExtention
    {
        /// <summary>
        /// Add transaction repository and dbContext.
        /// </summary>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddTransactionRepository(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            return services.AddTransactionRepository<TransactionRepository, AccountContext>(optionsAction);
        }

        /// <summary>
        /// Add custom blocks repository and dbContext.
        /// </summary>
        /// <typeparam name="TTransactionRepositary">The type of blocks repositary to be registered.</typeparam>
        /// <typeparam name="TTransactionDbContext">The type of context service to be registered.</typeparam>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddTransactionRepository<TTransactionRepositary, TTransactionDbContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TTransactionRepositary : TransactionRepository
            where TTransactionDbContext : AccountContext
        {
            services.AddDbContextPool<TTransactionDbContext>(optionsAction);
            services.TryAddTransient<TransactionRepository>();

            return services;
        }

    }
}
