﻿using System;
using System.Collections.Generic;

namespace Bastion.Accounts.Models
{
    public partial class AccountStatus
    {
        public AccountStatus()
        {
            Account = new HashSet<Account>();
        }

        public char AccountStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
