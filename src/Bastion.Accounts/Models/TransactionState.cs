﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bastion.Accounts.Models
{
    public partial class TransactionState
    {
        public TransactionState()
        {
            Transaction = new HashSet<Transaction>();
        }

        [Key]
        public char TransactionStateId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }

        public virtual ICollection<Transaction> Transaction { get; set; }
    }
}
