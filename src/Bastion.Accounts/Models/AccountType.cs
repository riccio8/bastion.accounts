﻿using System.Collections.Generic;

namespace Bastion.Accounts.Models
{
    public partial class AccountType
    {
        public AccountType()
        {
            Account = new HashSet<Account>();
            CanSendFromAccountType = new HashSet<CanSend>();
            CanSendToAccountType = new HashSet<CanSend>();
            CanTransferFromAccountType = new HashSet<CanTransfer>();
            CanTransferToAccountType = new HashSet<CanTransfer>();
        }

        public string AccountTypeId { get; set; }
        public string ImageUrl { get; set; }
        public short? SortOrder { get; set; }
        public bool? OurAccount { get; set; }

        public virtual ICollection<Account> Account { get; set; }
        public virtual ICollection<CanSend> CanSendFromAccountType { get; set; }
        public virtual ICollection<CanSend> CanSendToAccountType { get; set; }
        public virtual ICollection<CanTransfer> CanTransferFromAccountType { get; set; }
        public virtual ICollection<CanTransfer> CanTransferToAccountType { get; set; }
    }
}
