﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bastion.Accounts.Models
{
    public partial class TransactionType
    {
        public TransactionType()
        {
            Transaction = new HashSet<Transaction>();
        }

        [Key]
        public char TransactionTypeId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }

        public virtual ICollection<Transaction> Transaction { get; set; }
    }
}
