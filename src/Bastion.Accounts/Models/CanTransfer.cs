﻿namespace Bastion.Accounts.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("can_transfer", Schema = "transaction")]
    public partial class CanTransfer
    {
        [Key]
        [Column("from_account_type_id")]
        [StringLength(25)]
        public string FromAccountTypeId { get; set; }

        [Key]
        [Column("to_account_type_id")]
        [StringLength(25)]
        public string ToAccountTypeId { get; set; }

        [ForeignKey("FromAccountTypeId")]
        [InverseProperty("CanTransferFromAccountType")]
        public virtual AccountType FromAccountType { get; set; }

        [ForeignKey("ToAccountTypeId")]
        [InverseProperty("CanTransferToAccountType")]
        public virtual AccountType ToAccountType { get; set; }
    }
}
