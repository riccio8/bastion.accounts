﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Bastion.Accounts.Models
{
    public partial class TransactionDetail
    {
        public TransactionDetail()
        {
            TransactionEntry = new HashSet<TransactionEntry>();
        }

        public TransactionDetail(string comment, 
            string userId, 
            string orderId = null, 
            string tradeId = null, 
            decimal? price = null,
            string userComment = null) :  this()
        {
            this.Comment = comment;
            this.UserId = userId;
            this.FiatAmountSubtotal = 0;
            this.FiatAmountTotal = 0;
            this.FiatFee = 0;
            this.UserComment = userComment;
            this.Price = price;
        }

        [Key]
        [Column(TypeName = "varchar(255)")]
        public string TransactionDetailId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string TransactionId { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string UserId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string OrderId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string TradeId { get; set; }

        [Column(TypeName = "numeric(18,15)")]
        public decimal? Price { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? FiatAmountSubtotal { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal FiatFee { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? FiatAmountTotal { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Comment { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string UserComment { get; set; }

        public virtual Transaction Transaction { get; set; }
        public virtual ICollection<TransactionEntry> TransactionEntry { get; set; }

        public TransactionDetail Clone()
        {
            return new TransactionDetail()
            {
                TransactionDetailId = this.TransactionDetailId,
                TransactionId = this.TransactionId,
                UserId = this.UserId,
                OrderId = this.OrderId,
                TradeId = this.TradeId,
                Price = this.Price,
                FiatAmountSubtotal = this.FiatAmountSubtotal,
                FiatFee = this.FiatFee,
                FiatAmountTotal = this.FiatAmountTotal,
                Comment = this.Comment,
                TransactionEntry = this.TransactionEntry.Select(x => x.Clone()).ToList()
            };
        }

        public void AddEntry(TransactionEntry transactionEntry)
        {
            transactionEntry.TransactionDetailId = this.TransactionDetailId;
            this.TransactionEntry.Add(transactionEntry);
        }
    }
}
