﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bastion.Accounts.Models
{
    public class Currency
    {
        public Currency()
        {
            Account = new HashSet<Account>();
        }

        [Key]
        public string CurrencyId { get; set; }
        public string Name { get; set; }
        public char Symbol { get; set; }
        public short DecimalPoints { get; set; }
        public int? UiOrder { get; set; }
        public string Formatter { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
