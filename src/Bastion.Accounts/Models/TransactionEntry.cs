﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bastion.Accounts.Models
{
    public partial class TransactionEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OperationId { get; set; }
        public char TransactionType { get; set; }

        [ForeignKey("Account")]
        [Column(TypeName = "varchar(20)")]
        public string AccountId { get; set; }
        [Column(TypeName = "numeric(15,15)")]
        public decimal Amount { get; set; }

        [ForeignKey("OpposingAccount")]
        [Column(TypeName = "varchar(20)")]
        public string OpposingAccountId { get; set; }
        [Column(TypeName = "numeric(18,15)")]
        public decimal PrevBalance { get; set; }
        [Column(TypeName = "numeric(18,15)")]
        public decimal NewBalance { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string TransactionDetailId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Account OpposingAccount { get; set; }
        public virtual TransactionDetail TransactionDetail { get; set; }

        public TransactionEntry()
        {

        }

        public TransactionEntry(string accountId, decimal amount, char type, decimal prevBalance = 0, decimal newBalance = 0, string opposingAccountId = null)
        {
            this.AccountId = accountId;
            this.Amount = amount;
            this.TransactionType = type;
            this.PrevBalance = prevBalance;
            this.NewBalance = newBalance;
            this.OpposingAccountId = opposingAccountId;
        }

        public TransactionEntry Clone()
        {
            return new TransactionEntry()
            {
                OperationId = this.OperationId,
                TransactionType = this.TransactionType,
                AccountId = this.AccountId,
                Amount = this.Amount,
                OpposingAccountId = this.OpposingAccountId,
                PrevBalance = this.PrevBalance,
                NewBalance = this.NewBalance,
                TransactionDetailId = this.TransactionDetailId
            };
        }
    }
}
