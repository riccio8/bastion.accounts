﻿namespace Bastion.Accounts.Models
{
    using Bastion.Accounts.Transactions;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("transaction")]
    public partial class Transaction
    {
        public Transaction()
        {
            TransactionDetail = new HashSet<TransactionDetail>();
        }

        private int NextDetailNumber;


        [Key]
        [Column(TypeName = "varchar(255)")]
        public string TransactionId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Description { get; set; }
        public DateTime Ts { get; set; }
        public char TransactionTypeId { get; set; }
        [Column(TypeName = "numeric(18,15)")]
        public decimal? TotalFee { get; set; }
        public char TransactionStateId { get; set; }
        public int? Confirmations { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string NetworkTransactionHash { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Source { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Destination { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string PaymentMethod { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string ReferenceTransactionId { get; set; }

        [Column(TypeName = "varchar(25)")]
        public string FeeId { get; set; }

        public virtual TransactionState TransactionState { get; set; }
        public virtual TransactionType TransactionType { get; set; }
        public virtual ICollection<TransactionDetail> TransactionDetail { get; set; }


        public Transaction(string description, char transactionStateId, char transactionTypeId, TransactionInfo transactionInfo)
        {
            this.Description = description;
            this.TransactionStateId = transactionStateId;
            this.TransactionTypeId = transactionTypeId;
            this.Ts = DateTime.UtcNow;
            this.TransactionId = TransactionRepository.NewID();
            this.TransactionDetail = new List<TransactionDetail>();
            this.Destination = transactionInfo?.Destination;
            this.Source = transactionInfo?.Source;
            this.NetworkTransactionHash = transactionInfo?.TransactionHash;
            this.Confirmations = transactionInfo?.Confirmations;
            this.NextDetailNumber = 1;
        }


        public Transaction Clone()
        {
            return new Transaction()
            {
                TransactionId = this.TransactionId,
                Description = this.Description,
                Ts = this.Ts,
                TransactionTypeId = this.TransactionTypeId,
                TotalFee = this.TotalFee,
                TransactionStateId = this.TransactionStateId,
                Confirmations = this.Confirmations,
                NetworkTransactionHash = this.NetworkTransactionHash,
                Source = this.Source,
                Destination = this.Destination,
                PaymentMethod = this.PaymentMethod,
                TransactionDetail = this.TransactionDetail.Select(x => x.Clone()).ToList()
            };
        }

        public string GetNextDetailId()
        {
            return $"{this.TransactionId}-{this.NextDetailNumber++}";
        }
    }
}
