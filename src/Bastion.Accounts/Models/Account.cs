﻿namespace Bastion.Accounts.Models
{
    using Bastion.Accounts.ExtraModels;

    using System;

    public partial class Account
    {
        public Account()
        {
            //TransactionEntry = new HashSet<TransactionEntry>();
        }

        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string AccountTypeId { get; set; }
        /// <summary>
        /// Account number.
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Account iban.
        /// </summary>
        public string AccountIban { get; set; }
        public string Name { get; set; }

        public decimal Round(decimal amount)
        {
            return decimal.Round(amount, (short)this.Currency.DecimalPoints);
        }

        public decimal ExtraRound(decimal? amount)
        {
            if (amount == null)
            {
                return 0;
            }

            return decimal.Round(amount.Value, (short)this.Currency.DecimalPoints + 2);
        }

        public string GetStringAmount(decimal? amount)
        {
            if (amount == null)
            {
                return null;
            }
            return this.Currency.Symbol + amount.Value.ToString(this.Currency.Formatter);
        }

        public char? PaymentMethodStatusId { get; set; }
        public string CurrencyId { get; set; }
        public string ImageUrl { get; set; }
        public decimal? BtcBalance { get; set; }
        public decimal? EurBalance { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? LatestActivity { get; set; }
        public bool? IsPrimary { get; set; }
        public string BillAccountId { get; set; }
        public string CardAcquiredOriginalTransactionId { get; set; }

        public virtual AccountType AccountType { get; set; }
        public virtual AccountStatus PaymentMethodStatus { get; set; }
        public virtual Currency Currency { get; set; }
        //public virtual ICollection<TransactionEntry> TransactionEntry { get; set; }


        public Amount GetAmount(decimal amount)
        {
            return new Amount(this.CurrencyId, amount, this.Currency?.DecimalPoints);
        }
    }
}
