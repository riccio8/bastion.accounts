﻿namespace Bastion.Accounts.Models
{
    using Bastion.Accounts.Transactions;
    using System;

    public class Execution
    {
        public string OrderId { get; set; }
        public string TradeId { get; set; }
        public string UserId { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        public bool IsBuy { get; set; }
        public string Symbol { get; set; }
        public DateTime Ts { get; set; }

        public int Multiplier
        {
            get
            {
                if (IsBuy)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        public TradeType TradeType { get; set; }

        public string GetActionName(TransactionStatus transactionStatus)
        {
            if (this.IsBuy && transactionStatus == TransactionStatus.Completed)
                return "Bought";
            if (this.IsBuy && transactionStatus == TransactionStatus.Pending)
                return "Buy";
            if (!this.IsBuy && transactionStatus == TransactionStatus.Completed)
                return "Sold";
            if (!this.IsBuy && transactionStatus == TransactionStatus.Pending)
                return "Sell";
            throw new Exception($"Unknown transaction status {transactionStatus}");
        }
    }


    public enum TradeType
    {
        FixCoin,
        FixFiat,
        Limit
    }
}

