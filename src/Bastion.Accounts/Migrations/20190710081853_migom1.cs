﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Bastion.Accounts.Migrations
{
    public partial class migom1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "transaction");

            migrationBuilder.CreateTable(
                name: "fee",
                schema: "transaction",
                columns: table => new
                {
                    fee_id = table.Column<string>(type: "varchar(255)", nullable: false),
                    name = table.Column<string>(type: "varchar(255)", nullable: true),
                    @fixed = table.Column<decimal>(name: "fixed", nullable: false),
                    percent = table.Column<decimal>(nullable: false),
                    fixed_template = table.Column<string>(type: "varchar", nullable: true),
                    percent_template = table.Column<string>(type: "varchar", nullable: true),
                    admin_template = table.Column<string>(type: "varchar", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fee", x => x.fee_id);
                });

            migrationBuilder.CreateTable(
                name: "transaction_state",
                schema: "transaction",
                columns: table => new
                {
                    transaction_state_id = table.Column<char>(nullable: false),
                    name = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transaction_state", x => x.transaction_state_id);
                });

            migrationBuilder.CreateTable(
                name: "transaction_type",
                schema: "transaction",
                columns: table => new
                {
                    transaction_type_id = table.Column<char>(nullable: false),
                    name = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transaction_type", x => x.transaction_type_id);
                });

            migrationBuilder.CreateTable(
                name: "transaction",
                schema: "transaction",
                columns: table => new
                {
                    transaction_id = table.Column<string>(type: "varchar(255)", nullable: false),
                    description = table.Column<string>(type: "varchar(255)", nullable: true),
                    ts = table.Column<DateTime>(nullable: false),
                    transaction_type_id = table.Column<char>(nullable: false),
                    total_fee = table.Column<decimal>(type: "numeric(18,15)", nullable: true),
                    transaction_state_id = table.Column<char>(nullable: false),
                    confirmations = table.Column<int>(nullable: true),
                    network_transaction_hash = table.Column<string>(type: "varchar(255)", nullable: true),
                    source = table.Column<string>(type: "varchar(255)", nullable: true),
                    destination = table.Column<string>(type: "varchar(255)", nullable: true),
                    payment_method = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transaction", x => x.transaction_id);
                    table.ForeignKey(
                        name: "fk_transaction_transaction_state_transaction_state_id",
                        column: x => x.transaction_state_id,
                        principalSchema: "transaction",
                        principalTable: "transaction_state",
                        principalColumn: "transaction_state_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_transaction_transaction_type_transaction_type_id",
                        column: x => x.transaction_type_id,
                        principalSchema: "transaction",
                        principalTable: "transaction_type",
                        principalColumn: "transaction_type_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transaction_detail",
                schema: "transaction",
                columns: table => new
                {
                    transaction_detail_id = table.Column<string>(type: "varchar(255)", nullable: false),
                    transaction_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    user_id = table.Column<string>(type: "varchar(36)", nullable: true),
                    order_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    trade_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    price = table.Column<decimal>(type: "numeric(18,15)", nullable: true),
                    fiat_amount_subtotal = table.Column<decimal>(type: "numeric(18,2)", nullable: true),
                    fiat_fee = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    fiat_amount_total = table.Column<decimal>(type: "numeric(18,2)", nullable: true),
                    comment = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transaction_detail", x => x.transaction_detail_id);
                    table.ForeignKey(
                        name: "fk_transaction_detail_transaction_transaction_id",
                        column: x => x.transaction_id,
                        principalSchema: "transaction",
                        principalTable: "transaction",
                        principalColumn: "transaction_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "transaction_entry",
                schema: "transaction",
                columns: table => new
                {
                    operation_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    transaction_type = table.Column<char>(nullable: false),
                    asset_id = table.Column<string>(type: "varchar(20)", nullable: true),
                    amount = table.Column<decimal>(type: "numeric(15,15)", nullable: false),
                    opposing_asset_id = table.Column<string>(type: "varchar(20)", nullable: true),
                    prev_balance = table.Column<decimal>(type: "numeric(18,15)", nullable: false),
                    new_balance = table.Column<decimal>(type: "numeric(18,15)", nullable: false),
                    transaction_detail_id = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transaction_entry", x => x.operation_id);
                    table.ForeignKey(
                        name: "fk_transaction_entry_transaction_detail_transaction_detail_id",
                        column: x => x.transaction_detail_id,
                        principalSchema: "transaction",
                        principalTable: "transaction_detail",
                        principalColumn: "transaction_detail_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "transaction",
                table: "fee",
                columns: new[] { "fee_id", "admin_template", "fixed", "fixed_template", "name", "percent", "percent_template" },
                values: new object[,]
                {
                    { "DEPOSIT", null, 1m, "Deposit minimal fixed fee $<VALUE>", "Deposit", 0m, "Deposit fee, <VALUE>%" },
                    { "TRAN_OUT", null, 0m, "Outgoing fixed fee $<VALUE>", "Outgoing", 0.5m, "Outgoing fee, <VALUE>%" },
                    { "INVEST", null, 0m, "Invest fixed fee $<VALUE>", "Invest", 1m, "Invest fee, <VALUE>%" },
                    { "TRD_MAKER", null, 0m, "Maker fixed fee $<VALUE>", "Maker", 0m, "Maker fee, <VALUE>%" },
                    { "TRAN_IN", null, 22m, "Incoming fixed fee $<VALUE>", "Incoming", 1m, "Incoming fee, <VALUE>%" },
                    { "WDRW", null, 25m, "Withdraw minimal fee $<CALCULATED_FEE>", "Withdraw", 0.9m, "Withdraw fee $<CALCULATED_FEE> (<PERCENT_FEE>%)" },
                    { "MNTD", null, 0m, "No fee", "Minted tokens", 0m, "No fee" },
                    { "BTC_DEPOSIT", null, 0m, "BTC deposit fee $<VALUE>", "BTC deposit", 1m, "BTC deposit fee $<CALCULATED_FEE> (<PERCENT_FEE>%)" },
                    { "NO_FEE", null, 0m, "No fee", "No fee", 0m, "No fee" },
                    { "TRD_TAKER", null, 0m, "Taker fixed fee $<VALUE>", "Taker", 1m, "Taker fee, <VALUE>%" }
                });

            migrationBuilder.InsertData(
                schema: "transaction",
                table: "transaction_state",
                columns: new[] { "transaction_state_id", "name" },
                values: new object[,]
                {
                    { 'R', "Rejected" },
                    { 'C', "Completed" },
                    { 'P', "Pending" }
                });

            migrationBuilder.InsertData(
                schema: "transaction",
                table: "transaction_type",
                columns: new[] { "transaction_type_id", "name" },
                values: new object[,]
                {
                    { 'D', "Deposit" },
                    { 'W', "Withdraw" },
                    { 'B', "Buy" },
                    { 'S', "Sell" },
                    { 'F', "Fee" },
                    { 'E', "Exchange" },
                    { 'M', "Minted tokens" },
                    { 'R', "Receive" },
                    { 'N', "Send" },
                    { 'X', "Refund" },
                    { 'I', "Send token to bot" }
                });

            migrationBuilder.CreateIndex(
                name: "ix_transaction_transaction_state_id",
                schema: "transaction",
                table: "transaction",
                column: "transaction_state_id");

            migrationBuilder.CreateIndex(
                name: "ix_transaction_transaction_type_id",
                schema: "transaction",
                table: "transaction",
                column: "transaction_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_transaction_detail_transaction_id",
                schema: "transaction",
                table: "transaction_detail",
                column: "transaction_id");

            migrationBuilder.CreateIndex(
                name: "ix_transaction_entry_transaction_detail_id",
                schema: "transaction",
                table: "transaction_entry",
                column: "transaction_detail_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "fee",
                schema: "transaction");

            migrationBuilder.DropTable(
                name: "transaction_entry",
                schema: "transaction");

            migrationBuilder.DropTable(
                name: "transaction_detail",
                schema: "transaction");

            migrationBuilder.DropTable(
                name: "transaction",
                schema: "transaction");

            migrationBuilder.DropTable(
                name: "transaction_state",
                schema: "transaction");

            migrationBuilder.DropTable(
                name: "transaction_type",
                schema: "transaction");
        }
    }
}
