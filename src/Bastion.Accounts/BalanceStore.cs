﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Context;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Transactions;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    internal class BalanceStore
    {
        private static string pending = ((char)TransactionStatus.Pending).ToString();
        private static string rejected = ((char)TransactionStatus.Rejected).ToString();
        private static string completed = ((char)TransactionStatus.Completed).ToString();
        private static string canceled = ((char)TransactionStatus.Canceled).ToString();
        private readonly List<Transaction> pendingTransactions;
        private readonly object syncRoot = new object();


        //  AccountContext context;
        string testDbName;
        public BalanceStore(string testDbName = null)
        {
            this.testDbName = testDbName;
            pendingTransactions = ReadPendingTransactions(testDbName).Result;
        }

        private async Task<List<Transaction>> ReadPendingTransactions(string testDbName)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.Transaction
                    .Include(x => x.TransactionDetail)
                    .Include("TransactionDetail.TransactionEntry")
                    .Where(x => x.TransactionStateId.ToString() == pending);
                return await query.ToListAsync();
            }
        }

        private readonly Dictionary<string, Dictionary<string, decimal>> userBalances = new Dictionary<string, Dictionary<string, decimal>>();

        /// <summary>
        /// returns current balance snapshot, client shouldn't be able to update it.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, decimal>> GetBalanceSnapshot(string userId)
        {
            return await GetBalance(userId, true);
        }

        /// <summary>
        /// if snapshot is true - return clone of internal dictionary, otherwise return the dictionary itself.
        /// Always return dictionary clone from public methods, otherwise library clients can modify its internal state
        /// from other threads
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snapshot"></param>
        /// <returns></returns>
        private async Task<Dictionary<string, decimal>> GetBalance(string userId, bool snapshot = false)
        {
            //Dictionary<string, decimal> userBalance;
            //lock (this.syncRoot)
            //{
            //    if (userBalances.TryGetValue(userId, out userBalance))
            //    {
            //        return userBalance;
            //    }
            //}

            Dictionary<string, decimal> userBalance = await GetBalanceFromDb(userId);

            lock (this.syncRoot)
            {
                userBalances[userId] = userBalance;
                if(snapshot)
                    userBalance = new Dictionary<string, decimal>(userBalance);
            }

            return userBalance;
        }

        public async Task<Dictionary<string, decimal>> GetPotentialBalance(string userId)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionEntry.Include("TransactionDetail.Transaction").Where(x => x.TransactionDetail.UserId == userId
                && (x.TransactionDetail.Transaction.TransactionStateId.ToString() == completed
                || x.TransactionDetail.Transaction.TransactionStateId.ToString() != pending));

                var result = query.GroupBy(x => x.AccountId)
                    .Select(gr => new
                    {
                        AssetId = gr.Key,
                        Balance = gr.Sum(y => y.Amount)
                    });

                return await result.ToDictionaryAsync(x => x.AssetId, x => x.Balance);
            }
        }

        internal async Task<IReadOnlyDictionary<string, decimal>> GetFullBalance(string userId)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionEntry.Include("TransactionDetail.Transaction").Where(x => x.TransactionDetail.UserId == userId
                && x.TransactionDetail.Transaction.TransactionStateId.ToString() == completed);

                var result = query.GroupBy(x => x.AccountId)
                    .Select(gr => new
                    {
                        AssetId = gr.Key,
                        Balance = gr.Sum(y => y.Amount)
                    });

                return await result.ToDictionaryAsync(x => x.AssetId, x => x.Balance);
            }
        }

        public async Task<decimal> GetBalanceValue(string userId, string accountId)
        {
            var userBalance = await GetBalanceSnapshot(userId);
            if (userBalance.TryGetValue(accountId, out decimal amount))
            {
                return amount;
            }

            return 0m;
        }

        private async Task<Dictionary<string, decimal>> GetBalanceFromDb(string userId)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionEntry.Include("TransactionDetail.Transaction").Where(x => x.TransactionDetail.UserId == userId
                && (x.TransactionDetail.Transaction.TransactionStateId.ToString() == completed
                     || (x.TransactionDetail.Transaction.TransactionStateId.ToString() == pending && x.Amount < 0)));

                var result = query.GroupBy(x => x.AccountId)
                    .Select(gr => new
                    {
                        AssetId = gr.Key,
                        Balance = gr.Sum(y => y.Amount)
                    });

                return await result.ToDictionaryAsync(x => x.AssetId, x => x.Balance);
            }
        }

        public Dictionary<string, decimal> GetAllBalance()
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionEntry.Where(x => x.TransactionDetail.Transaction.TransactionStateId.ToString() != rejected
                    && !(x.TransactionDetail.Transaction.TransactionStateId.ToString() == pending && x.Amount > 0));
                var result = query.GroupBy(y => new { y.TransactionDetail.UserId, y.AccountId })
                    .Select(gr => new
                    {
                        gr.Key.AccountId,
                        Balance = gr.Sum(y => y.Amount)
                    });

                return result.ToDictionary(x => x.AccountId, y => y.Balance);
            }
        }

        public async Task UpdateBalance(Transaction transaction)
        {
            lock (this.syncRoot)
            {
                //Если обновление транзакции
                //Если транзакция Reject - вернуть деньги
                //Если pending или completed - обновить баланс на разницу
                var initTransaction = pendingTransactions.FirstOrDefault(x => x.TransactionId == transaction.TransactionId);
                if (initTransaction != null)
                {
                    foreach (var detail in transaction.TransactionDetail)
                    {
                        var initDetail = initTransaction.TransactionDetail.First(x => x.TransactionDetailId == detail.TransactionDetailId);
                        var userBalance = GetBalance(detail.UserId).Result;
                        foreach (var entry in detail.TransactionEntry)
                        {
                            var initEntry = initDetail.TransactionEntry.First(x => x.OperationId == entry.OperationId);
                            if (!userBalance.ContainsKey(entry.AccountId))
                            {
                                userBalance.TryAdd(entry.AccountId, 0);
                            }
                            if (transaction.TransactionStateId == (char)TransactionStatus.Rejected && entry.Amount < 0)
                            {
                                userBalance[entry.AccountId] += Math.Abs(entry.Amount);
                            }
                            if (transaction.TransactionStateId == (char)TransactionStatus.Completed)
                            {
                                if (entry.Amount < 0)
                                {
                                    userBalance[entry.AccountId] += entry.Amount - initEntry.Amount;
                                }
                                else
                                {
                                    userBalance[entry.AccountId] += entry.Amount;
                                }

                            }
                            if (transaction.TransactionStateId == (char)TransactionStatus.Pending && entry.Amount < 0)
                            {
                                userBalance[entry.AccountId] += entry.Amount - initEntry.Amount;
                            }
                        }
                    }
                    pendingTransactions.Remove(initTransaction);
                    if (transaction.TransactionStateId == (char)TransactionStatus.Pending)
                    {
                        pendingTransactions.Add(transaction.Clone());
                    }
                }
                else
                {
                    foreach (var detail in transaction.TransactionDetail)
                    {
                        var userBalance = GetBalance(detail.UserId).Result;
                        foreach (var entry in detail.TransactionEntry)
                        {
                            if (!userBalance.ContainsKey(entry.AccountId))
                            {
                                userBalance.TryAdd(entry.AccountId, 0);
                            }
                            if (transaction.TransactionStateId == (char)TransactionStatus.Completed
                                || (transaction.TransactionStateId == (char)TransactionStatus.Pending && entry.Amount <= 0))
                            {
                                userBalance[entry.AccountId] += entry.Amount;
                            }
                        }
                    }
                    if (transaction.TransactionStateId == (char)TransactionStatus.Pending)
                    {
                        pendingTransactions.Add(transaction.Clone());
                    }
                }
            }
            
        }

    }
}
