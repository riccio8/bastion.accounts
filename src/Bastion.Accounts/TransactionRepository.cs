﻿using System;

namespace Bastion.Accounts
{
    using Bastion.Accounts.Context;
    using Bastion.Accounts.Enums;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Stores;
    using Bastion.Accounts.Transactions;
    using Bastion.Helper;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Memory;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public partial class TransactionRepository: Repository<AccountContext>
    {
        private readonly Regex regEx = new Regex("'\\w+'");
        private readonly BalanceStore balanceStore;

        public string[] AccountTypesFilter { get; set; } = new string[0];

        private readonly CurrencyStore currencyStore;
        private readonly AccountsStore accountsStore;
        string testDbName = null;
        public TransactionRepository(AccountsStore accountsStore, CurrencyStore currencyStore, AccountContext context) : base(context)
        {
            this.currencyStore = currencyStore;
            this.accountsStore = accountsStore;
            this.testDbName = context.testDbName;
            this.balanceStore = new BalanceStore(context.testDbName);
        }

        public TransactionRepository(AccountsStore accountsStore, CurrencyStore currencyStore) : base()
        {
            this.currencyStore = currencyStore;
            this.accountsStore = accountsStore;
            this.balanceStore = new BalanceStore();
        }


        public async Task<Account> GetAccount(string accountId)
        {
            if (string.IsNullOrWhiteSpace(accountId))
            {
                throw new Exception($"AccountId is empty. ({nameof(TransactionRepository)}.{nameof(GetAccount)}");
            }
            if (!accountsStore.TryGetAccount(accountId, out Account account))
            {
                account = await this.GetEntity<Account>(x => x.AccountId == accountId, nameof(AccountType), nameof(Currency));
                if (account == null)
                {
                    throw new Exception($"Account '{account}' is not exist. ({nameof(TransactionRepository)}.{nameof(GetAccount)}");
                }
                accountsStore.AddAccount(account);
            }
            return account;
        }

        public async Task<Currency> GetCurrency(string currencyId)
        {
            if (string.IsNullOrWhiteSpace(currencyId))
            {
                throw new Exception($"Currency id is empty.");
            }
            if (!currencyStore.TryGetCurrency(currencyId, out Currency currency))
            {
                currency = await this.GetEntity<Currency>(x => x.CurrencyId == currencyId);
                if (currency == null)
                {
                    throw new Exception($"Currency '{currency}' not supported.");
                }
                currencyStore.AddCurrency(currency);
            }
            return currency;
        }

        public async Task<Account> GetPrimaryAccount(string userId)
        {
            return await this.GetEntity<Account>(x => x.UserId == userId && x.IsPrimary == true, nameof(AccountType), nameof(Currency));
        }

        public async Task<Account> GetAccountByNumber(string accountNumber)
        {
            return await this.GetEntity<Account>(x => x.AccountNumber == accountNumber, nameof(AccountType), nameof(Currency));
        }

        public async Task<IEnumerable<Account>> GetAccounts(string userId)
        {
            return await this.GetEntities<Account>(x => x.UserId == userId, nameof(AccountType), nameof(Currency));
        }

        public async Task<IEnumerable<Account>> GetAllAccounts(string[] currencies, string[] accountTypes, int page = 1, int perPage = 20, SortDirection sortDirection = SortDirection.Asc, SortBy sortBy = SortBy.SortOrder)
        {
            IQueryable<Account> request = this.context.Account.Include(x => x.AccountType).Include(x => x.Currency)
                .Where(x => currencies.Contains(x.CurrencyId) && accountTypes.Contains(x.AccountTypeId))
                .Skip((page - 1) * perPage).Take(perPage);
            request = MakeSortOrder(sortDirection, sortBy, request);
            return await request.ToListAsync();
        }

        public async Task<IEnumerable<Account>> GetAllAccounts(string currency = null, int page = 1, int perPage = 20, SortDirection sortDirection = SortDirection.Asc, SortBy sortBy = SortBy.SortOrder, params string[] accountTypes)
        {
            IQueryable<Account> request = this.context.Account.Include(x => x.AccountType).Include(x => x.Currency);
            if (!string.IsNullOrWhiteSpace(currency))
            {
                request = request.Where(x => x.CurrencyId == currency);
            }
            if (accountTypes != null && accountTypes.Length != 0)
            {
                request = request.Where(x => accountTypes.Contains(x.AccountTypeId));
            }
            request = request.Skip((page - 1) * perPage).Take(perPage);
            request = MakeSortOrder(sortDirection, sortBy, request);
            return await request.ToListAsync();
        }


        private static IQueryable<Account> MakeSortOrder(SortDirection sortDirection, SortBy sortBy, IQueryable<Account> request)
        {
            switch (sortBy)
            {
                case SortBy.SortOrder:
                    if (sortDirection == SortDirection.Asc)
                        request = request.OrderBy(x => x.AccountType.SortOrder);
                    else
                        request = request.OrderByDescending(x => x.AccountType.SortOrder);
                    break;
                case SortBy.UserId:
                    if (sortDirection == SortDirection.Asc)
                        request = request.OrderBy(x => x.UserId);
                    else
                        request = request.OrderByDescending(x => x.UserId);
                    break;
                case SortBy.AccountType:
                    if (sortDirection == SortDirection.Asc)
                        request = request.OrderBy(x => x.AccountTypeId);
                    else
                        request = request.OrderByDescending(x => x.AccountTypeId);
                    break;
                case SortBy.Currency:
                    if (sortDirection == SortDirection.Asc)
                        request = request.OrderBy(x => x.CurrencyId);
                    else
                        request = request.OrderByDescending(x => x.CurrencyId);
                    break;
                default:
                    throw new Exception($"Unknown sort order: {sortBy}");
            }

            return request;
        }

        public Dictionary<string, decimal> GetAllBalance()
        {
            return balanceStore.GetAllBalance();
        }

        public async Task<IReadOnlyDictionary<string, decimal>> GetUserAvailableBalance(string userId)
        {

            Dictionary<string, decimal> balance = await this.balanceStore.GetBalanceSnapshot(userId);
            Dictionary<string, decimal> returnedBalance = new Dictionary<string, decimal>();
            foreach (string accountId in balance.Keys)
            {
                Account account = await this.GetAccount(accountId);
                returnedBalance.Add(accountId, account.Round(balance[accountId]));
            }
            return returnedBalance;
        }

        public async Task<IReadOnlyDictionary<string, decimal>> GetUserPotentialBalance(string userId)
        {
            return await this.balanceStore.GetPotentialBalance(userId);
        }

        public async Task<IReadOnlyDictionary<string, decimal>> GetUserFullBalance(string userId)
        {
            return await this.balanceStore.GetFullBalance(userId);
        }

        public async Task<decimal> GetBalance(string accountId)
        {
            Account account = await this.GetAccount(accountId);
            return await this.balanceStore.GetBalanceValue(account.UserId, accountId);
        }

        public async Task<decimal?> GetMasterAccountBalance()
        {
            using (var context = new AccountContext(testDbName))
            {
                var sum = await context.Transaction.SumAsync(x => x.TotalFee);
                return sum;
            }
        }

        

        public async Task<decimal> GetSoldVolume(string assetId, decimal price)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionEntry.Include(x => x.TransactionDetail)
                    .Where(x => x.Amount > 0 && x.AccountId == assetId.ToUpperInvariant() && x.TransactionDetail.Price == price);
                var result = await query.SumAsync(x => x.Amount);
                return result;
            }
        }

        public async Task<InvestStat> GetInvestStat(string assetId)
        {
            using (var context = new AccountContext(testDbName))
            {
                var list = await context.TransactionEntry.Include(x => x.TransactionDetail)
                    .Where(x => x.AccountId == assetId).ToListAsync();
                list = list.Where(x => x.TransactionType == 'B').ToList();
                return new InvestStat()
                {
                    Amount = list.Sum(x => x.Amount),
                    USD = list.Sum(x => x.Amount * x.TransactionDetail.Price)
                };
            }
        }



        public async Task<IEnumerable<TransactionDetail>> GetTransactions(string accountId, DateTime? from = null, DateTime? to = null)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.TransactionDetail
                    .Include(x => x.TransactionEntry)
                    .Include(x => x.Transaction)
                    .Include("TransactionEntry.OpposingAccount.Currency")
                    .Include("TransactionEntry.Account.Currency")
                    .Where(x => x.TransactionEntry.Any(y => y.AccountId == accountId)
                    && (!from.HasValue || x.Transaction.Ts >= from.Value)
                    && (!to.HasValue || x.Transaction.Ts < to.Value));
                return await query.ToListAsync();
            }
        }

        public TransactionDetail GetLastTransaction(string accountId, DateTime? to = null)
        {
            using (var context = new AccountContext(testDbName))
            {
                return context.TransactionDetail
                    .Include(x => x.TransactionEntry)
                    .Include(x => x.Transaction)
                    .Include("TransactionEntry.OpposingAccount.Currency")
                    .Include("TransactionEntry.Account.Currency")
                    .OrderBy(x => x.Transaction.Ts)
                    .LastOrDefault(x => x.TransactionEntry.Any(y => y.AccountId == accountId)
                    && (!to.HasValue || x.Transaction.Ts < to.Value));
            }
        }

        public TransactionDetail GetFirstTransaction(string accountId, DateTime? from = null)
        {
            using (var context = new AccountContext(testDbName))
            {
                return context.TransactionDetail
                    .Include(x => x.TransactionEntry)
                    .Include(x => x.Transaction)
                    .Include("TransactionEntry.OpposingAccount.Currency")
                    .Include("TransactionEntry.Account.Currency")
                    .OrderBy(x => x.Transaction.Ts)
                    .FirstOrDefault(x => x.TransactionEntry.Any(y => y.AccountId == accountId)
                    && (!from.HasValue || x.Transaction.Ts > from.Value));
            }
        }

        public async Task<Transaction> GetTransaction(string transactionId)
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.Transaction
                    .Include(x => x.TransactionDetail)
                    .Include("TransactionDetail.TransactionEntry.Account")
                    .Where(x => x.TransactionId == transactionId);
                return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<Transaction> GetLastTransaction()
        {
            using (var context = new AccountContext(testDbName))
            {
                var query = context.Transaction
                    .Include(x => x.TransactionDetail)
                    .Include("TransactionDetail.TransactionEntry")
                    .OrderByDescending(x => x.Ts);
                return await query.FirstOrDefaultAsync();
            }
        }


        public static string NewID(int length = 8)
        {
            return Randomizer.GenerateRandomString(8).ToUpperInvariant();
        }

        public Account GetAccountByCurrencyId(string userId, string currencyId)
        {
            var account = this.GetEntity<Account>(x => x.UserId == userId && x.CurrencyId == currencyId
                                                                      && (AccountTypesFilter.Length == 0 || AccountTypesFilter.Contains(x.AccountType.AccountTypeId)), nameof(AccountType), nameof(Currency)).Result;
            if (account == null)
            {
                throw new Exception($"User {userId} doesn't have {currencyId} account of type {String.Join(",", AccountTypesFilter)}");
            }

            return account;
        }
    }
}
