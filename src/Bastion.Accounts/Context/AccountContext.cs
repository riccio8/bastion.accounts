﻿namespace Bastion.Accounts.Context
{
    using Bastion.Accounts.Constants;
    using Bastion.Accounts.Extensions;
    using Bastion.Accounts.Models;
    using Bastion.Kernel;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class AccountContext : DbContext
    {
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountStatus> AccountStatus { get; set; }
        public virtual DbSet<AccountType> AccountType { get; set; }
        public virtual DbSet<CanSend> CanSend { get; set; }
        public virtual DbSet<CanTransfer> CanTransfer { get; set; }
        public virtual DbSet<Transaction> Transaction { get; set; }
        public virtual DbSet<TransactionDetail> TransactionDetail { get; set; }
        public virtual DbSet<TransactionEntry> TransactionEntry { get; set; }
        public virtual DbSet<TransactionState> TransactionState { get; set; }
        public virtual DbSet<TransactionType> TransactionType { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }


        public AccountContext()
        {
        }

        internal string testDbName = null;
        public bool IsTestEnvironment { get { return testDbName != null; } }

        public AccountContext(string testDbName)
        {
            if(testDbName != null)
                this.testDbName = testDbName;
        }

        public AccountContext(DbContextOptions<AccountContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!IsTestEnvironment)
            {
                if (!optionsBuilder.IsConfigured)
                {
                    var connectionString = CfgKernel.Configuration["DbConnection"];
                    if (connectionString == null)
                        throw new Exception("Missing config section DbConnection");
                    optionsBuilder.UseNpgsql(connectionString);
                }
            }
            else
            {
                optionsBuilder.UseInMemoryDatabase(testDbName);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("transaction");

            modelBuilder.Entity<CanSend>(entity =>
            {
                entity.HasKey(e => new { e.FromAccountTypeId, e.ToAccountTypeId });
            });

            modelBuilder.Entity<CanTransfer>(entity =>
            {
                entity.HasKey(e => new { e.FromAccountTypeId, e.ToAccountTypeId });
            });

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetName().ToSnakeCase());
                }
            }

            modelBuilder.Entity<TransactionType>()
                .HasData(GetTransactionTypeData());

            modelBuilder.Entity<TransactionState>()
                .HasData(GetTransactionStateData());
        }


        public static TransactionType[] GetTransactionTypeData()
        {
            return new TransactionType[]
            {
                new TransactionType()
                {
                    TransactionTypeId = 'D',
                    Name = "Deposit",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'W',
                    Name = "Withdraw",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'B',
                    Name = "Buy",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'S',
                    Name = "Sell",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'F',
                    Name = "Fee",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'E',
                    Name = "Exchange",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'M',
                    Name = "Minted tokens",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'R',
                    Name = "Receive",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'N',
                    Name = "Send",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'X',
                    Name = "Refund",
                },
                new TransactionType()
                {
                    TransactionTypeId = 'I',
                    Name = "Send token to bot",
                }
            };
        }

        public static TransactionState[] GetTransactionStateData()
        {
            return new TransactionState[]
            {
                new TransactionState()
                {
                    TransactionStateId = 'P',
                    Name = "Pending",
                },
                new TransactionState()
                {
                    TransactionStateId = 'C',
                    Name = "Completed",
                },
                new TransactionState()
                {
                    TransactionStateId = 'R',
                    Name = "Rejected",
                }
            };
        }

    }
}
