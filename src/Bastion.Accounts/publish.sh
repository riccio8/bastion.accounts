cd bin/Debug
latest=$(ls -t *.nupkg | head -n1)
echo "trying to publish latest nupkg: $latest"
dotnet nuget push $latest -k "$NUGET_BASTION_KEY" -s https://api.nuget.org/v3/index.json
