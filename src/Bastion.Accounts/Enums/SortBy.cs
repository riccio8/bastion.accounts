﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Enums
{
    public enum SortBy
    {
        SortOrder,
        UserId,
        AccountType,
        Currency
    }
}
