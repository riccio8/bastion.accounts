﻿namespace Bastion.Accounts.Transactions
{
    public enum TransactionStatus
    {
        Completed = 'C',
        Pending = 'P',
        Rejected = 'R',
        Canceled = 'X'
    }
}
