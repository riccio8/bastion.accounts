﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Enums
{
   public enum SortDirection
    {
        Asc,
        Desc
    }
}
