﻿namespace Bastion.Accounts.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;

    public class FeeInfo
    {
        public IFeeCalculate FeeCalculate { get; set; }
        public string TragetAccounId { get; set; }
        public string FeeId { get; set; }
        public IList<CollectorInfo> Collectors { get; set; }

        public FeeInfo(IEnumerable<CollectorInfo> collectors, string targetAccountId, IFeeCalculate feeCalculate, string FeeId)
        {
            this.FeeCalculate = feeCalculate;
            this.TragetAccounId = targetAccountId;
            this.FeeId = FeeId;
            this.Collectors = collectors.ToList();
        }
    }
}
