﻿namespace Bastion.Accounts.Interfaces
{
    public interface IFeeCalculate
    {
        decimal Calculate(decimal amount);
    }
}
