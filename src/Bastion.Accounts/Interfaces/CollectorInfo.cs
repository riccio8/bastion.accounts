﻿namespace Bastion.Accounts.Interfaces
{
    public class CollectorInfo
    {
        public string AccountId { get; set; }
        public decimal Value { get; set; }
        public string Comment { get; set; }

        public CollectorInfo(string accountId, decimal value, string comment = null)
        {
            this.AccountId = accountId;
            this.Value = value;
            this.Comment = comment;
        }
    }
}
