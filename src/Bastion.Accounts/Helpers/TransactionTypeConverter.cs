﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Helpers
{
    public static class TransactionTypeConverter
    {
        public static string GetString(char typeTransaction)
        {
            switch (typeTransaction)
            {
                case TransactionTypeChar.Deposit:
                    return "Deposit";
                case TransactionTypeChar.Withdraw:
                    return "Withdraw";
                case TransactionTypeChar.Fee:
                    return "Fee";
                case TransactionTypeChar.Receive:
                    return "Receive";
                case TransactionTypeChar.Refund:
                    return "Refund";
                case TransactionTypeChar.Send:
                    return "Send";
                case TransactionTypeChar.Sell:
                    return "Sell";
                case TransactionTypeChar.Buy:
                    return "Buy";
                case TransactionTypeChar.Transfer:
                    return "Transfer";
                case TransactionTypeChar.Pay:
                    return "Pay";
                case TransactionTypeChar.Exchange:
                    return "Exchange";
                default:
                    return "Unknown";
            }
        }
    }
}
