﻿namespace Bastion.Accounts
{
    using Bastion.Accounts.Models;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Options;

    class Program
    {
        static void Main(string[] args)
        {
            Bastion.Kernel.CfgKernel.ConfigureApplication();
            TransactionRepository transactionRepository = new TransactionRepository(new Stores.AccountsStore(), new Stores.CurrencyStore());
            //var result = transactionRepository.GetEntities<Currency>().Result;
            //var account = transactionRepository.GetEntity<Account>(x => true, nameof(Currency)).Result;
            //var account2 = transactionRepository.GetAccount("KOL-M").Result;
            var reports = transactionRepository.GetTransactionReport("KOL-M").Result;
        }
    }
}
