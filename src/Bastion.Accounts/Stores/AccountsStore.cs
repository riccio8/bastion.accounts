﻿using Bastion.Accounts.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Stores
{
    public class AccountsStore
    {
        private ConcurrentDictionary<string, Account> accounts;

        public AccountsStore()
        {
            accounts = new ConcurrentDictionary<string, Account>();
        }

        public bool TryGetAccount(string accountId, out Account account)
        {
            return accounts.TryGetValue(accountId, out account);
        }

        public void AddAccount(Account account)
        {
            accounts.TryAdd(account.AccountId, account);
        }
    }
}
