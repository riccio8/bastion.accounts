﻿using Bastion.Accounts.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Bastion.Accounts.Stores
{
    public class CurrencyStore
    {
        private ConcurrentDictionary<string, Currency> currencies;

        public CurrencyStore()
        {
            currencies = new ConcurrentDictionary<string, Currency>();
        }

        public bool TryGetCurrency(string currencyId, out Currency currency)
        {
            return currencies.TryGetValue(currencyId, out currency);
        }

        public void AddCurrency(Currency currency)
        {
            currencies.TryAdd(currency.CurrencyId, currency);
        }
    }
}
