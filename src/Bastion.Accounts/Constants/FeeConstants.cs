﻿namespace Bastion.Accounts.Constants
{
    public static class FeeConstants
    {
        public const string InvestFeeId = "INVEST";
        public const string TradeFeeId = "TRD_TAKER";
        public const string TradeFeeForBrokerId = "TRD_FOR_BROKER";
        public const string TradeMakerFeeId = "TRD_MAKER";
        public const string WithdrowFeeId = "WDRW";
        public const string DepositFeeId = "DEPOSIT";
        public const string DepositBtcFeeId = "BTC_DEPOSIT";
        public const string TransferInconingFeeId = "TRAN_IN";
        public const string TransferOutgoingFeeId = "TRAN_OUT";
        public const string MintedTokenFee = "MNTD";
        public const string NoFee = "NO_FEE";
    }
}
