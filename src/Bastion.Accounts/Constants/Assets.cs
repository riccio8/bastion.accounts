﻿namespace Bastion.Accounts.Constants
{
    public static class Assets
    {
        public const string UsdAssetId = "USD";
        public const string EurAssetId = "EUR";
        public const string GbpAssetId = "GBP";

        public static bool IsFiatAsset(string assetId)
        {
            switch (assetId)
            {
            case UsdAssetId:
            case EurAssetId:
            case GbpAssetId:
                return true;
            default:
                return false;
            }
        }

        public static string GetIcon(string assetId)
        {
            switch (assetId)
            {
            case UsdAssetId:
                return "$";
            case EurAssetId:
                return "€";
            case GbpAssetId:
                return "£";
            default:
                return assetId;
            }
        }
    }
}
