namespace Bastion.Accounts
{
    public class TransactionTypeChar
    {

        public const char Deposit = 'D';

        public const char Withdraw = 'W';

        public const char Buy = 'B';

        public const char Sell = 'S';

        public const char Fee = 'F';

        public const char Exchange = 'E';

        public const char Receive = 'R';

        public const char Send = 'N';

        public const char Refund = 'X';

        public const char Transfer = 'T';

        public const char Pay = 'P';
    }
}