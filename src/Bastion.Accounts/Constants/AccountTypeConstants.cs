﻿namespace Bastion.Accounts.Constants
{
    using Bastion.Accounts.Models;

    public static class AccountTypeConstants
    {
        public static string Dimm = "Dimm";
        public static string DimmInternal = "DimmInternal";
        public static string MyBank = "MyBank";
        public static string MigomIban = "Migom";
        public static string Contact = "Contact";
        public static string MyMigomCard = "MigomCard";
        public static string ExternalCard = "ExternalCard";
        public static string BillRecipient = "Bill/Recipient";

        public static bool IsDimmAccount(this Account account) => account.AccountTypeId == Dimm;
        public static bool IsDimmInternalAccount(this Account account) => account.AccountTypeId == DimmInternal;
        public static bool IsMyBankAccount(this Account account) => account.AccountTypeId == MyBank;
        public static bool IsMigomIbanAccount(this Account account) => account.AccountTypeId == MigomIban;
        public static bool IsMyMigomCardAccount(this Account account) => account.AccountTypeId == MyMigomCard;
        public static bool IsExternalCardAccount(this Account account) => account.AccountTypeId == ExternalCard;
        public static bool IsBillRecipientAccount(this Account account) => account.AccountTypeId == BillRecipient;
        public static bool IsUserAccount(this Account account) => account.AccountTypeId == Dimm
                                                                || account.AccountTypeId == MyBank
                                                                || account.AccountTypeId == MigomIban
                                                                || account.AccountTypeId == MyMigomCard;

    }
}
